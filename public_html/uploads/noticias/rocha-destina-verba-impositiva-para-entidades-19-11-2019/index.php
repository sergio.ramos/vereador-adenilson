<!-- Desenvolvido por Marlon Gomes -->
<?
include("inc/include.php");
?>
<!DOCTYPE html>
<html>
	<head>
		<title>.: Vereador Adenilson Rocha - PSDB :.</title>
		<meta charset="utf-8">
		<link rel="icon" type="image/ico" href="<? echo $link_site; ?>img/fav.png">
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<link rel="stylesheet" type="text/css" href="<? echo $link_site; ?>css/estilo.css">
		<link rel="stylesheet" type="text/css" href="<? echo $link_site; ?>css/perfect-scrollbar.css">
		<link rel="stylesheet" type="text/css" href="<? echo $link_site; ?>css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="<? echo $link_site; ?>css/anji.css">
		<script src="<? echo $link_site; ?>js/jquery-3.1.1.js"></script>
		<script type="text/javascript" src="<? echo $link_site; ?>js/bootstrap.min.js"></script>
		<script type="text/javascript" src="<? echo $link_site; ?>js/scrollreveal.min.js"></script>
		<script type="text/javascript" src="<? echo $link_site; ?>js/responsiveslides.min.js"></script>
		<script type="text/javascript" src="<? echo $link_site; ?>js/perfect-scrollbar.jquery.js"></script>
		<script type="text/javascript" src="<? echo $link_site; ?>js/jquery.mask.min.js"></script>
   		<script type="text/javascript" src="<? echo $link_site; ?>js/form.js"></script>
   		<script type="text/javascript">
			$(function($) {
			 $('.contato_form').submit(function() {
			  $('#gif').css("opacity", 1);
			  $(".contato_submit").attr("disabled","disabled");
			  $(this).ajaxSubmit(function(resposta) {
			   if (resposta){
			    $('#gif').css("opacity", 0);
			    $('div.mensagem_form').html(resposta);
			    $(".contato_submit").removeAttr("disabled");
			   }
			  });
			  return false;
			 });
			});
		</script>
		<script type="text/javascript">
	    	jQuery(document).ready(function($) { 
	        $(".scroll").click(function(event){        
	            event.preventDefault();
	            $('html,body').animate({scrollTop:$(this.hash).offset().top - 150},800);
	       		});
	   		 });
	    </script>
		<?php include_once("config/google.php") ?>
	</head>
	<body>
		<div id="topo"></div>
		<div id="container">
			<div class="linha"></div> <!-- linha gradiente -->
			<div class="logo">
				<div class="alinhamento">
					<a href="http://adenilsonrocha.com.br/"><img src="<? echo $link_site; ?>img/logo.png"></a>
				</div>
			</div>
			<div class="slide_home">
				<div class="slide">
					<ul class="rslides">
						<?
							$slider=mysql_query("SELECT * FROM slide");
							while($slide = mysql_fetch_array($slider)) {
						?>
						<li><img src="http://adenilsonrocha.com.br/uploads/<? echo $slide[imagem] ?>"></li>
						<? } ?>
					</ul>
				</div>
			</div>
			<div class="linha"></div> <!-- linha gradiente -->
			<div class="biografia">
				<div class="alinhamento">
					<div class="img_aden" data-anijs="if: scroll, on: window, do: fadeInUp    animated, before: scrollReveal, after: holdAnimClass">
						<img src="<? echo $link_site; ?>img/RECORTE_10.png">
					</div>
					<div class="text_biog">
						<h1>BIOGRAFIA</h1>
						<img src="<? echo $link_site; ?>img/RECORTE_07.png"><br>
						<div class="ul_cobertura">
						<?
							$busca=mysql_query("SELECT * FROM candidato");
							while($info = mysql_fetch_array($busca)) {
						?>
						<p><? echo nl2br($info[texto1]) ?></p>
						<? } ?>
						</div>
					</div>
				</div>
			</div>
			<div class="linha"></div> <!-- linha gradiente -->
			<div class="noticias">
				<div class="alinhamento">
					<h1>NOTÍCIAS</h1>
					<div class="alinha">
						<a href="<? echo $link_site; ?>noticias"><h2>VER TODAS</h2></a>
						<h3></h3>
					</div>
					<ul class="ul_not">
						<?
		                $Query=mysql_query("SELECT * FROM coberturas WHERE imagem_capa <> '' ORDER BY id DESC LIMIT 3");
		                while($Dados = mysql_fetch_array($Query)) {
			            $titulo_cob = $Dados[titulo];
						$img = $Dados[imagem_capa];
			            ?>
						<a href="../noticia/<? echo $Dados['link']; ?>">
							<li class="not_peq" data-anijs="if: scroll, on: window, do: fadeInUp animated, before: scrollReveal, after: holdAnimClass">
								<div class="hoverzoom1">
									<img src="/uploads/ResizeBasic.php?filename=<? echo $img; ?>&width=318&height=230"  width="318" height="230"/>
								</div>
								<p><? echo $titulo_cob; ?></p>
							</li>
						</a>
						<? } ?>
					</ul>
				</div>
			</div>
			<div class="redes_sociais">
				<div class="alinhamento">
					<h1>Redes Sociais</h1>
					<ul>
						<li class="sr-icons">
							<a href="https://www.facebook.com/vereadoradenilsonrocha" target="_blank"><img src="img/icones_03.png" id="face"></a>
						</li>
						<li class="sr-icons">
							<a href="https://twitter.com/adenilson_rocha" target="_blank"><img src="img/icones_05.png" id="twitter"></a>
						</li>
						<li class="sr-icons">
							<a data-toggle="modal" data-target="#myModal"><img src="img/icones_07.png" id="snap"></a>
						</li>
						<li class="sr-icons">
							<a href="https://www.instagram.com/adenilsonrochaoficial/" target="_blank"><img src="img/icones_09.png" id="insta"></a>
						</li>
					</ul>
					<div id="myModal" class="modal fade" role="dialog">
					  <div class="modal-dialog">
					    <div class="modal-content">
					      <div class="modal-body">
					      <button type="button" class="close" data-dismiss="modal">&times;</button>
					        <p><img src="img/snap.png"></p>
					      </div>
					    </div>
					  </div>
					</div>
				</div>
			</div>
			<div class="contato">
				<div class="alinhamento">
					<h1>CONTATO</h1>
					<form class="contato_form" action="<? echo $link_site; ?>sendemail.php?local=contato" method="post" data-anijs="if: scroll, on: window, do: bounceInLeft animated, before: scrollReveal, after: holdAnimClass">
						<div class="alinha_form">
							<p>Nome:</p>
							<input type="text" name="nome" id="input_form">
						</div>
						<div class="alinha_form">
							<p>E-mail:</p>
							<input type="email" name="form_email" id="input_form">
						</div>
						<br><br>
						<div class="alinha_form">
							<p>Telefone:</p>
							<input name="telefone" type="text" class="contato_form_input fmtelefone" id="input_form" />
							<script type="text/javascript">
								$('.fmtelefone').mask('(00) 0000-00009');
								$('.fmtelefone').blur(function(event) {
									if($(this).val().length == 15){
										$('.fmtelefone').mask('(00) 00000-0009');
									} else {
										$('.fmtelefone').mask('(00) 0000-00009');
									}
								});
							</script>
						</div>
						<div class="alinha_form">
							<p>Assunto:</p>
							<input type="text" name="assunto" id="input_form">
						</div>
						<br><br>
						<p>Mensagem:</p>
						<textarea id="text_input" name="mensagem"></textarea>
						<br><br>
						<button type="submit" id="ENVIO">Enviar</button>
						<button type="reset" id="ENVIO" class="contato_submit">Limpar</button>
						<img id="gif" src="<? echo $link_site; ?>img/loading.gif">
						<div class="mensagem_form"></div>
					</form>
					<div class="caixa_lado sr-icons">
						<?
							$Query=mysql_query("SELECT * FROM dados_site WHERE id = '1'");
							while($Dados = mysql_fetch_array($Query)) {
						?>
						<h2>Outros canais de comunicação</h2>
						<h3>PELOS FONES:</h3>
						<h4><? echo $Dados[telefones]; ?></h4>
						<h3>NOSSO ENDEREÇO:</h3>
						<div class="mapa">
							<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d580.4464240096426!2d-55.5108900883878!3d-11.851888525316783!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x845f5600e75de13b!2sC%C3%A2mara+de+Vereadores+de+Sinop!5e0!3m2!1spt-BR!2sbr!4v1483989287643" width="380" height="220" frameborder="0" style="border:0" allowfullscreen></iframe>
						</div>
						<? } ?>
					</div>
				</div>
			</div>
		</div> <!-- container -->
		<? include("footer.php"); ?>
		<script>
			$(function() {
	    		$(".rslides").responsiveSlides({
	    		nav: true,
	    	});
	 		});
		</script>
		<!-- Scroolbar -->
        <script type="text/javascript">
            $(document).ready(function(){
                $('.ul_cobertura').perfectScrollbar();
                });
        </script>
        <!-- Efeito movimento redes sociais -->
        <script type="text/javascript" src="<? echo $link_site; ?>js/creative.min.js"></script>
        <script src="<? echo $link_site; ?>js/anijs-min.js"></script>
		<script src="<? echo $link_site; ?>js/anijs-helper-scrollreveal.js"></script>	
	</body>
</html>