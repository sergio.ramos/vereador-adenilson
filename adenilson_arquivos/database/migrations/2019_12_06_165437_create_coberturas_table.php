<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoberturasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coberturas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('data_cobertura');
            $table->string('titulo');
            $table->string('link');
            $table->longText('descricao');
            $table->string('redator');
            $table->string('local');
            $table->dateTime('data_cadastro');
            $table->string('codigo');
            $table->string('tags');
            $table->string('tags_link');
            $table->string('imagem_capa');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coberturas');
    }
}
