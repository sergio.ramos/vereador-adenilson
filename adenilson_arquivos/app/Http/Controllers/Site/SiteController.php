<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Candidato;
use App\Models\Coberturas;
use App\Models\Edicao;
use App\Models\Slides;
use App\Models\Videos;

// emails models
use App\Models\Mensagens;

// config email use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContatoMail;

class SiteController extends Controller
{
    public function index()
    {
        $noticias = Coberturas::all();
        $slides = Slides::all();
        $videos = Videos::all();

        foreach ($noticias as $key => $value) {
            // $value->descricao = strip_tags($value->descricao);
            // $value->titulo = strip_tags($value->titulo);
            // $value->descricao = utf8_decode($value->descricao);
            // $value->titulo = utf8_decode($value->titulo);
        }

        // dd($videos);

        foreach ($videos as $key => $value) {
            if(!empty($value->videos)) {
                $videosArray = explode(';', $value->videos);
                array_pop($videosArray);
                $value->videos = $videosArray;
            }
        }

        // dd($videos[$videos->count()-1]);

        $data = array(  
            'titulo_site' => $_SESSION['site']['titulo_site'],
            'dados_site' => unserialize($_SESSION['site']['dados_site']),
            'noticias' => $noticias,
            'slides' => $slides,
            'video' => $videos[$videos->count()-1]
        );

        return view('site.index', $data);
    }

    public function biografia()
    {
        $biografia = Candidato::find(1);
        $biografia->titulo = strip_tags($biografia->titulo);
        $biografia->titulo = utf8_decode($biografia->titulo);
        $biografia->texto = utf8_decode($biografia->texto);

        $data = array(  
            'titulo_site' => 'Biografia',
            'dados_site' => unserialize($_SESSION['site']['dados_site']),
            'biografia' => $biografia
        );
        
        return view('site.biografia', $data);
    }

    public function noticias()
    {
        $noticias = Coberturas::all();
        $noticias = $noticias->sortByDesc('data');

        // dd(utf8_decode($coberturasUTF8[0]['descricao']));

        $noticias_slice = $noticias->slice(0, 9);

        $data = array(  
            'titulo_site' => 'Na mídia',
            'dados_site' => unserialize($_SESSION['site']['dados_site']),
            'noticias_slice' => $noticias_slice,
            'noticias' => $noticias
        );
        
        return view('site.listarNoticias', $data);
    }

    public function maisNoticias()
    {

        $noticias = Coberturas::all();
        $noticias = $noticias->sortByDesc('data');

        $row = $_POST['row'];

        $noticias_slice = $noticias->slice($row, 9);

        $html = '';

        foreach($noticias_slice as $key => $value){

            $id = $value->id;
            $foto = $value->imagem_capa;
            $titulo = $value->titulo;
            $link = $value->link;
            $fotografo = $value->fotografo;
            $link_site = 'http://adenilson/';
            
            $html .= '<div class="quadro post" id="post_'.$id.'">';
            $html .= '<a href="'.$link_site.'noticias'.'/'.$link.'/'.$id.'" class="not-link">';
            $html .= '<div class="img-noticia" style="background:url(uploads/noticias/'.$link.'/'.$foto.') center center;background-size:cover;">';
            $html .= '<div class="hover-img">';
            $html .= '<i class="fa fa-newspaper-o" style="font-size:60px;color:#fff">';
            $html .= '</i>';
            $html .= '</div>';
            $html .= '</div>';
            $html .= '<div class="not-t">';
            $html .= '<span>'.$titulo.'</span>';
            $html .= '</div>';
            $html .= '</a>';
            $html .= '</div>';
        }

        return $html;
    }

    public function verNoticia(Request $request)
    {
        $noticia = Coberturas::find($request->id);
        // $noticia->descricao = utf8_decode($noticia->descricao);

        $data = array(  
            'titulo_site' => $noticia->titulo,
            'dados_site' => unserialize($_SESSION['site']['dados_site']),
            'noticia' => $noticia
        );

        return view('site.noticia', $data);
    }

    public function cafepolitica() 
    {
        $videos = Videos::all();

        foreach ($videos as $key => $value) {
            // $value->titulo = utf8_decode($value->titulo);
            // $value->texto = utf8_decode($value->texto);
        }
        
        $data = array(  
            'titulo_site' => 'Café com política',
            'dados_site' => unserialize($_SESSION['site']['dados_site']),
            'videos' => $videos
        );

        return view('site.cafepolitica', $data);
    }

    public function maisVideos()
    {

        $videos = Videos::all();
        $videos = $videos->sortByDesc('data');

        $row = $_POST['row'];

        $videos_slice = $videos->slice($row, 9);

        $html = '';

        foreach($videos_slice as $key => $value){
            $value->titulo = utf8_decode($value->titulo);

            $id = $videos->id;
            $foto = $videos->imagem_capa;
            $titulo = $videos->titulo;
            $link = $videos->link;
            $fotografo = $videos->fotografo;
            $link_site = 'http://adenilsonrocha/';
            
            $html .= '<div class="quadro" id="post_'.$id.'">';
            $html .= '<a href="'.$link_site.'video.php?id='.$link.'" class="not-link">';
            $html .= '<div class="img-noticia" style="background:url(uploads/ResizeBasic.php?filename='.$foto.'&width=500&height=500&add=) center center;background-size:cover;">';
            $html .= '<div class="hover-img-video">';
            $html .= '<i class="fa fa-video-camera" style="font-size:60px;color:#fff">';
            $html .= '</i>';
            $html .= '</div>';
            $html .= '<div class="hover-img">';
            $html .= '<i class="fa fa-eye" style="font-size:60px;color:#fff">';
            $html .= '</i>';
            $html .= '</div>';
            $html .= '</div>';
            $html .= '<div class="not-t">';
            $html .= '<span>'.$titulo.'</span>';
            $html .= '</div>';
            $html .= '</a>';
            $html .= '</div>';
        }

        return $html;
    }

    public function verVideo($link, $id)
    {
        $video = Videos::find($id);
        // $video->descricao = utf8_decode($video->descricao);
        $video->data_video = $this->convertDate('%d de %B de %Y ás ', date('Y-m-d H:i:s', strtotime($video['created_at'])));

        if(!empty($video->videos)) {
            $videos = explode(';', $video->videos);
            array_pop($videos);
            $video->videos = $videos;
        }

        // dd($video->videos);

        $data = array(  
            'titulo_site' => $video->titulo,
            'dados_site' => unserialize($_SESSION['site']['dados_site']),
            'video' => $video
        );

        return view('site.video', $data);
    }

    public function edicao() 
    {
        $edicoes = Edicao::all();

        foreach ($edicoes as $key => $value) {
            $value->titulo = utf8_decode($value->titulo);
        }

        $data = array(  
            'titulo_site' => 'Edição Digital',
            'dados_site' => unserialize($_SESSION['site']['dados_site']),
            'edicoes' => $edicoes
        );

        return view('site.listarEdicoes', $data);
    }

    public function verEdicao(Request $request)
    {
        $edicao = Edicao::find($request->id);
        $edicao->titulo = utf8_decode($edicao->titulo);

        $data = array(  
            'titulo_site' => $edicao->titulo,
            'dados_site' => unserialize($_SESSION['site']['dados_site']),
            'edicao' => $edicao
        );

        return view('site.edicao', $data);
    }

    public function contato() 
    {
        $data = array(  
            'titulo_site' => 'Contato',
            'dados_site' => unserialize($_SESSION['site']['dados_site'])
        );

        return view('site.contato', $data);
    }

    public function sendContatoEmail(Request $request)
    {
        // dd($request->input());
        // autentication recaptcha
        $siteKey = "6LeGR84UAAAAAM2TFLs4A63z3dneZ6yHOo2XsAQc";
        $secretKey = "6LeGR84UAAAAAPEMQgaOue41F6CfcV9dnRxormOv";

        // if($request->input("g-recaptcha-response") != null && !empty($request->input("g-recaptcha-response"))) {
        //     $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . $secretKey . '&response=' . $request->input("g-recaptcha-response"));
        //     $responseData = json_decode($verifyResponse);

        //     if($responseData->success) {

                $contato_mensagem = new Mensagens;
                $nome = $request->input('nome');
                $email = $request->input('email');
                $telefone = $request->input('telefone');
                $assunto = $request->input('assunto');
                $mensagem = $request->input('mensagem');

                // Enviando o e-mail comercial@topsapp.com.br
                Mail::to('sergio.ramos@unemat.br')->send(new ContatoMail($nome, $email, $telefone, $assunto, $mensagem));

                $contato_mensagem->nome_mensagem = $nome;
                $contato_mensagem->email_mensagem = $email;
                // $contato_mensagem->site_mensagem = $request->input('site');
                $contato_mensagem->telefone_mensagem = $telefone;
                $contato_mensagem->assunto_mensagem = $assunto;
                $contato_mensagem->mensagem_mensagem = $mensagem;

                $contato_mensagem->save();

                return redirect()->route('site.contato')->with('mensagem', 'O e-mail foi enviado com sucesso! Retornaremos sua solicitação.');
        //     } else {
        //         return redirect()->route('site.contato')->with('invalido', 'A verificação do robô falhou, tente novamente.');
        //     }
        // } else {
        //     return redirect()->route('site.contato')->with('invalido', 'Por favor, verifique a caixa reCAPTCHA.');
        // }
    }
}
