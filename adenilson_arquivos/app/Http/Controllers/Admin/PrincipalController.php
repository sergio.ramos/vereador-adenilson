<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Titulos_site;

class PrincipalController extends Controller
{
    public function index()
    {
        $users = User::all();
        
        $tituloSite = Titulos_site::find(1);
        $tituloSite->atualizacao = $this->convertDate('%d de %B de %Y ás ', date('Y-m-d H:i:s', strtotime($tituloSite->updated_at)));

        foreach ($users as $key => $value) {
            // alterando formato de data e hora da visita
            $value->visita = $this->convertDate('%d de %B de %Y ás ', $value->visita);

            // alterando formato de data e hora do cadastro
            $value->data_cadastro = $this->convertDate('%d de %B de %Y ás ', date('Y-m-d H:i:s', strtotime($value->created_at)));
        }

        $data = array(  'usuario' => $_SESSION['usuario'],
                        'users' => $users,
                        'titulo_site' => $tituloSite
        );

        return view('admin.index', $data);
    }

    // funções para o banco de dados
    public function atualizarTitulo(Request $request) 
    {
        $titulo = $request->input('titulo_site');

        $tituloSite = Titulos_site::find(1);

        if($tituloSite === null) {
            
            $tituloSite = new Titulos_site;
            $tituloSite->tiulo_site = $titulo;
            $tituloSite->save();

            return redirect()->route('admin.home')->with('mensagem', 'O título da página foi cadastrado com sucesso!');
        } else if($tituloSite->titulo_site === $titulo) {

            return redirect()->route('admin.home')->with('mensagem', 'O título da página já está atualizado!');
        } else {
            
            $tituloSite->titulo_site = $titulo;
            $tituloSite->save();

            return redirect()->route('admin.home')->with('mensagem', 'O título da página foi atualizado com sucesso!');
        }
    }

    public function viewAlterarSenha() 
    {

        $data = array(  'usuario' => $_SESSION['usuario']);

        return view('admin.alterar_senha', $data);
    }
    
    public function alterarSenha(Request $request) 
    {
        $user = User::find($_SESSION['usuario']['id']);

        if(password_verify($request->input('senha_atual'), $user->password)) {
            
            $user->password = bcrypt($request->input('nova_senha'));
            $user->save();

            return redirect()->route('alterar.senha')->with('mensagem', 'A senha foi alterada com sucesso!');
        } else {

            return redirect()->route('alterar.senha')->with('invalido', 'Senha atual incorreta!');
        }
    }
}
