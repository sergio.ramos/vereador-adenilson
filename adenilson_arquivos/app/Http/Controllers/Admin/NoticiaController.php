<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Coberturas;
use URLify;

class NoticiaController extends Controller
{
    public function noticias()
    {
        $noticias = Coberturas::all();

        foreach ($noticias as $key => $value) {
        
            $value->data_cadastro = $this->convertDate('%d de %B de %Y ás ', date('Y-m-d H:i:s', strtotime($value->created_at)));
        }

        $data = array(  'usuario' => $_SESSION['usuario'],
                        'noticias' => $noticias
        );

        return view('admin.noticias.index', $data);
    }

    public function viewCadastrarNoticia() 
    {
        
        $data = array(  'usuario' => $_SESSION['usuario']);

        return view('admin.noticias.cadastrar_noticia', $data);
    }

    public function uploadImagens(Request $request)
    {
        if(!is_dir(public_path() . "/uploads/noticias/")) {

            mkdir(public_path() . "/uploads/noticias/");
        }

        $image = $request->file('file');
        $imageName = $image->getClientOriginalName();
        $image->move('uploads/noticias/', $imageName);

        return response()->json(['success' => 'arquivo enviado com sucesso', 'arquivo' => $imageName]);
    }

    public function deleteImagens(Request $request)
    {
        $filename = $request->get('filename');
        $path = public_path() . '/uploads/noticias/' . $filename;
        if (file_exists($path)) {
            unlink($path);
        }
        
        return $filename;
    }

    public function cadastrarNoticia(Request $request) 
    {

        $noticia = new Coberturas();
        $noticia->titulo = $request->input('titulo_noticia');
        $noticia->data = $request->input('data_noticia');
        $noticia->fotografo = $request->input('redator_noticia');
        $noticia->descricao = $request->input('descricao_noticia');
        $noticia->link = URLify::filter($request->input('titulo_noticia'), 300, 'br') . '-' . date('d-m-Y');

        if(!is_dir(public_path() . "/uploads/noticias/" . $noticia->link)) {

            mkdir(public_path() . "/uploads/noticias/" . $noticia->link);
        }

        if(!empty($request->file('foto_capa'))) {

            $noticia->imagem_capa = $this->noticiaUploadFile($request->file('foto_capa'), $noticia->link);
        }

        if(!empty($request->input('imagens'))) {

            if(!is_dir(public_path() . "/uploads/noticias/" . $noticia->link . "/fotos")) {

                mkdir(public_path() . "/uploads/noticias/" . $noticia->link . "/fotos");
            }

            $imagens = $request->input('imagens');
            $noticia->fotos = $imagens;
            $imagens = explode(";", $imagens);
            array_pop($imagens);

            foreach ($imagens as $key => $value) {
                if(is_dir(public_path() . "/uploads/noticias/" . $value)) {
                    rename(public_path() . "/uploads/noticias/" . $value, public_path() . "/uploads/noticias/" . $noticia->link . "/fotos" . "/" . $value);
                }
            }
        }

        $noticia->save();

        return redirect()->route('noticias')->with('mensagem', 'A notícia foi cadastrada com sucesso!');
    }

    public function viewEditarNoticia(Request $request) 
    {
        $noticia = Coberturas::find($request->id);

        $data = array(  'usuario' => $_SESSION['usuario'],
                        'noticia' => $noticia
        );

        return view('admin.noticias.editar_noticia', $data);
    }

    public function viewFilesNoticia(Request $request)
    {
        $noticia = Coberturas::find($request->id);
        $ds = DIRECTORY_SEPARATOR;
        $diretorio = public_path() . "/uploads/noticias/" . $noticia->link . "/fotos" . "/";
        $result  = array();
 
        $files = scandir($diretorio);                 
        if ( false!==$files ) {
            foreach ( $files as $file ) {
                if ( '.'!=$file && '..'!=$file) {
                    $obj['name'] = $file;
                    $obj['size'] = filesize($diretorio.$ds.$file);
                    $result[] = $obj;
                }
            }
        }
        
        header('Content-type: text/json');
        header('Content-type: application/json');
        return $result;
    }

    public function editarUploadImagens(Request $request)
    {
        $noticia = Coberturas::find($request->id);
        $image = $request->file('file');
        $imageName = $image->getClientOriginalName();
        $image->move('uploads/noticias/' . $noticia->link . "/fotos" . "/", $imageName);

        return response()->json(['success' => 'arquivo enviado com sucesso', 'arquivo' => $imageName]);
    }

    public function editarDeleteImagens(Request $request)
    {
        $noticia = Coberturas::find($request->id);
        $filename = $request->get('filename');
        $path = public_path() . '/uploads/noticias/' . $noticia->link . "/fotos" . "/" . $filename;
        $noticia->fotos = str_replace($filename . ";", "", $noticia->fotos);
        $noticia->save();
        if (file_exists($path)) {
            unlink($path);
        }
        
        return $filename;
    }

    public function editarNoticia(Request $request) 
    {
        $noticia = Coberturas::find($request->input('id'));

        if( $noticia->titulo === $request->input('titulo_noticia') &&
            $noticia->data === $request->input('data_noticia') &&
            $noticia->redator === $request->input('redator_noticia') &&
            $noticia->descricao === $request->input('descricao_noticia') &&
            $noticia->fotos === $request->input('imagens') &&
            ($noticia->imagem_capa === $request->input('foto_capa_name') || $request->input('foto_capa_name') == "Selecione um arquivo")
        ) {

            return redirect()->route('noticias')->with('mensagem', 'A noticia já está atualizada!');
        } else {

            $noticia->descricao = $request->input('descricao_noticia');
            $noticia->data = $request->input('data_noticia');
            $noticia->redator = $request->input('redator_noticia');

            // se não houver a pasta >> criar
            if(!is_dir(public_path() . "/uploads/noticias/" . $noticia->link)) {
                mkdir(public_path() . "/uploads/noticias/" . $noticia->link);
            }

            if(!empty($request->file('imagem_capa'))) {

                $noticia->imagem_capa = $this->noticiaUploadFile($request->file('imagem_capa'), $noticia->link);
            }
            
            // se titulo for diferente, trocar eles
            if($noticia->titulo != $request->input('titulo_noticia')) {

                $noticia->titulo = $request->input('titulo_noticia');
                $oldLink = $noticia->link;
                $noticia->link = URLify::filter($request->input('titulo_noticia'), 300, 'br') . '-' . date('d-m-Y', strtotime($noticia->created_at));
                rename(public_path() . "/uploads/noticias/" . $oldLink, public_path() . "/uploads/noticias/" . $noticia->link);
            }
            
            $noticia->fotos = $request->input('imagens');

            if(!empty($request->file('foto_capa')) && $noticia->imagem_capa != $request->input('foto_capa_name')) {

                unlink(public_path() . '/uploads/noticias/' . $noticia->link . '/' . $noticia->imagem_capa);
                $noticia->imagem_capa = $this->noticiaUploadFile($request->file('foto_capa'), $noticia->link);
            }

            $noticia->save();

            return redirect()->route('noticias')->with('mensagem', 'A noticia foi atualizada com sucesso!');
        }
    }

    public function deletarNoticia($id) 
    {
        $noticia = Coberturas::find($id);
        if(is_dir('uploads/noticias/' .  $noticia->link)) {
            NoticiaController::delTree('uploads/noticias/' .  $noticia->link);
        }

        $noticia->delete();

        return redirect()->route('noticias')->with('mensagem', 'A noticia foi deletada com sucesso!');
    }

    public function noticiaUploadFile($file, $link) {
        
        $file_name = $file->getClientOriginalName();
        $file_size = $file->getClientSize();
        $file_extension = $file->getClientOriginalExtension();
        $expensions = array('jpeg', 'jpg', 'png', 'gif');

        if(in_array($file_extension, $expensions) === false) {

            return redirect()->route('noticias')->with('invalido', 'Extensão não permitida, por favor escolha um arquivo JPEG ou PNG.');
        } else if($file_size > 2097152) {

            return redirect()->route('noticias')->with('invalido', 'O tamanho do arquivo não pode exceder 2 MB');
        } else {

            $file->move('uploads/noticias/' . $link . '/', $file_name);

            return $file_name;
        }
    }

    public static function delTree($dir) { 
        $files = array_diff(scandir($dir), array('.','..')); 
        foreach ($files as $file) { 
            (is_dir("$dir/$file")) ? NoticiaController::delTree("$dir/$file") : unlink("$dir/$file"); 
        } 
        return rmdir($dir);
    }

    function detect_encoding($string)
    {
        ////w3.org/International/questions/qa-forms-utf-8.html
        if (preg_match('%^(?: [\x09\x0A\x0D\x20-\x7E] | [\xC2-\xDF][\x80-\xBF] | \xE0[\xA0-\xBF][\x80-\xBF] | [\xE1-\xEC\xEE\xEF][\x80-\xBF]{2} | \xED[\x80-\x9F][\x80-\xBF] | \xF0[\x90-\xBF][\x80-\xBF]{2} | [\xF1-\xF3][\x80-\xBF]{3} | \xF4[\x80-\x8F][\x80-\xBF]{2} )*$%xs', $string))
            return 'UTF-8';
    
        //If you need to distinguish between UTF-8 and ISO-8859-1 encoding, list UTF-8 first in your encoding_list.
        //if you list ISO-8859-1 first, mb_detect_encoding() will always return ISO-8859-1.
        return mb_detect_encoding($string, array('UTF-8', 'ASCII', 'ISO-8859-1', 'JIS', 'EUC-JP', 'SJIS'));
    }
    
    function convert_encoding($string, $to_encoding, $from_encoding = '')
    {
        if ($from_encoding == '')
            $from_encoding = NoticiaController::detect_encoding($string);
    
        if ($from_encoding == $to_encoding)
            return $string;
    
        return mb_convert_encoding($string, $to_encoding, $from_encoding);
    }
}
