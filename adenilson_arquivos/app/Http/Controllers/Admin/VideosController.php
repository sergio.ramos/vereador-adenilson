<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Videos;
use URLify;

class VideosController extends Controller
{
    public function videos()
    {
        $videos = Videos::all();

        foreach ($videos as $key => $value) {

            $value->data_cadastro = $this->convertDate('%d de %B de %Y ás ', date('Y-m-d H:i:s', strtotime($value->created_at)));
        }

        $data = array(  'usuario' => $_SESSION['usuario'],
                        'videos' => $videos
        );

        return view('admin.videos.index', $data);
    }

    public function viewCadastrarVideo() 
    {
        $data = array(  'usuario' => $_SESSION['usuario']);

        return view('admin.videos.cadastrar_video', $data);
    }

    public function uploadVideos(Request $request)
    {
        if(!is_dir(public_path() . "/uploads/videos/")) {

            mkdir(public_path() . "/uploads/videos/");
        }

        $video = $request->file('file');
        $videoName = $video->getClientOriginalName();
        $video->move('uploads/videos/', $videoName);

        return response()->json(['success' => 'arquivo enviado com sucesso', 'arquivo' => $videoName]);
    }

    public function deleteVideos(Request $request)
    {
        $filename = $request->get('filename');
        $path = public_path() . '/uploads/videos/' . $filename;
        if (file_exists($path)) {
            unlink($path);
        }
        
        return $filename;
    }

    public function cadastrarVideo(Request $request) 
    {

        $video = new Videos();
        $video->titulo = $request->input('titulo_video');
        $video->data = $request->input('data_video');
        $video->descricao = $request->input('descricao_video');
        $video->link = URLify::filter($request->input('titulo_video'), 300 ,'br') . '-' . date('d-m-Y');

        if(!is_dir(public_path() . "/uploads/videos/" . $video->link)) {

            mkdir(public_path() . "/uploads/videos/" . $video->link);
        }

        if(!empty($request->file('foto_capa'))) {

            $video->imagem_capa = $this->videoUploadFile($request->file('foto_capa'), $video->link);
        }

        if(!empty($request->input('videos'))) {

            if(!is_dir(public_path() . "/uploads/videos/" . $video->link . "/videos")) {

                mkdir(public_path() . "/uploads/videos/" . $video->link . "/videos");
            }

            $videos = $request->input('videos');
            $video->videos = $videos;
            $videos = explode(";", $videos);
            array_pop($videos);

            foreach ($videos as $key => $value) {
                if(file_exists(public_path() . "/uploads/videos/" . $value)) {
                    rename(public_path() . "/uploads/videos/" . $value, public_path() . "/uploads/videos/" . $video->link . "/videos" . "/" . $value);
                }
            }
        }

        $video->save();

        return redirect()->route('videos')->with('mensagem', 'O vídeo foi cadastrado com sucesso!');
    }

    public function viewEditarVideo($id) 
    {
        $video = Videos::find($id);

        $data = array(  'usuario' => $_SESSION['usuario'],
                        'video' => $video
        );

        return view('admin.videos.editar_video', $data);
    }

    public function viewFilesVideo($id)
    {
        $video = Videos::find($id);
        $ds = DIRECTORY_SEPARATOR;
        $diretorio = public_path() . "/uploads/videos/" . $video->link . "/videos" . "/";
        $result  = array();
 
        $files = scandir($diretorio);                 
        if ( false!==$files ) {
            foreach ( $files as $file ) {
                if ( '.'!=$file && '..'!=$file) {
                    $obj['name'] = $file;
                    $obj['size'] = filesize($diretorio.$ds.$file);
                    $obj['img'] = $video->imagem_capa;
                    $result[] = $obj;
                }
            }
        }
        
        header('Content-type: text/json');
        header('Content-type: application/json');
        return $result;
    }

    public function editarUploadVideos(Request $request)
    {
        $video = Videos::find($request->id);
        $img = $video->imagem_capa;
        $videoFile = $request->file('file');
        $videoName = $videoFile->getClientOriginalName();
        $videoFile->move('uploads/videos/' . $video->link . "/videos" . "/", $videoName);

        return response()->json(['success' => 'arquivo enviado com sucesso', 'arquivo' => $videoName, 'img' => $img]);
    }

    public function editarDeleteVideos(Request $request)
    {
        $video = Videos::find($request->id);
        $filename = $request->get('filename');
        $path = public_path() . '/uploads/videos/' . $video->link . "/videos" . "/" . $filename;
        $video->videos = str_replace($filename . ";", "", $video->videos);
        $video->save();
        if (file_exists($path)) {
            unlink($path);
        }
        
        return $filename;
    }

    public function editarVideo(Request $request) 
    {
        $video = Videos::find($request->input('id'));

        if( $video->titulo === $request->input('titulo_video') &&
            $video->data === $request->input('data_video') &&
            ($video->imagem_capa === $request->input('foto_capa_name') || $request->input('foto_capa_name') == "Selecione um arquivo") &&
            $video->videos === $request->input('videos') &&
            $video->descricao === $request->input('descricao_video')
        ) {

            return redirect()->route('videos')->with('mensagem', 'O video já está atualizado!');
        } else {

            $video->descricao = $request->input('descricao_video');
            $video->data = $request->input('data_video');

            // se não houver a pasta >> criar
            if(!is_dir(public_path() . "/uploads/videos/" . $video->link)) {
                mkdir(public_path() . "/uploads/videos/" . $video->link);
            }

            if(!empty($request->file('imagem_capa'))) {

                $video->imagem_capa = $this->videoUploadFile($request->file('imagem_capa'), $video->link);
            }
            
            // se titulo for diferente, trocar eles
            if($video->titulo != $request->input('titulo_video')) {

                $video->titulo = $request->input('titulo_video');
                $oldLink = $video->link;
                $video->link = URLify::filter($request->input('titulo_video'), 300, 'br') . '-' . date('d-m-Y', strtotime($video->created_at));
                rename(public_path() . "/uploads/videos/" . $oldLink, public_path() . "/uploads/videos/" . $video->link);
            }
            
            $video->videos = $request->input('videos');

            if(!empty($request->file('foto_capa')) && $video->imagem_capa != $request->input('foto_capa_name')) {

                unlink(public_path() . '/uploads/videos/' . $video->link . '/' . $video->imagem_capa);
                $video->imagem_capa = $this->videoUploadFile($request->file('foto_capa'), $video->link);
            }

            $video->save();

            return redirect()->route('videos')->with('mensagem', 'O video foi atualizado com sucesso!');
        }
    }

    public function deletarvideo($id) 
    {
        $video = Videos::find($id);
        if(is_dir('uploads/videos/' .  $video->link)) {
            $this->delTree('uploads/videos/' .  $video->link);
        }

        $video->delete();

        return redirect()->route('videos')->with('mensagem', 'O video foi deletado com sucesso!');
    }

    public function videoUploadFile($file, $link) {
        
        $file_name = $file->getClientOriginalName();
        $file_size = $file->getClientSize();
        $file_extension = $file->getClientOriginalExtension();
        $expensions = array('jpeg', 'jpg', 'png', 'gif');

        if(in_array($file_extension, $expensions) === false) {

            return redirect()->route('videos')->with('invalido', 'Extensão não permitida, por favor escolha um arquivo JPEG ou PNG.');
        } else if($file_size > 2097152) {

            return redirect()->route('videos')->with('invalido', 'O tamanho do arquivo não pode exceder 2 MB');
        } else {

            $file->move('uploads/videos/' . $link . '/', $file_name);

            return $file_name;
        }
    }

    public static function delTree($dir) { 
        $files = array_diff(scandir($dir), array('.','..')); 
        foreach ($files as $file) { 
            (is_dir("$dir/$file")) ? VideosController::delTree("$dir/$file") : unlink("$dir/$file"); 
        } 
        return rmdir($dir);
    }
}
