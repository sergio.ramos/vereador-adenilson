<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Candidato;

class BiografiaController extends Controller
{
    public function index()
    {
        $biografia = Candidato::find(1);

        // dd($biografia->updated_at   );

        $biografia->atualizacao = $this->convertDate('%d de %B de %Y ás ', date('Y-m-d H:i:s', strtotime($biografia->updated_at)));

        $data = array(  'usuario' => $_SESSION['usuario'],
                        'biografia' => $biografia
        );

        return view('admin.biografia', $data);
    }

    public function atualizarDados(Request $request)
    {
        $biografia = Candidato::find(1);

        if (     $biografia->titulo === $request->input('titulo') && 
                $biografia->img === $request->input('file-name') &&
                $biografia->texto === $request->input('descricao')
        ) {

            return redirect()->route('biografia')->with('mensagem', 'Os dados da página já estão atualizados!');
        } else if($biografia->img === $request->input('file-name')){

            $biografia->titulo = $request->input('titulo');
            $biografia->texto = $request->input('descricao');
            $biografia->save();

            return redirect()->route('biografia')->with('mensagem', 'Os dados da página foram atualizados com sucesso!');
        } else {

            $file_name = $request->file->getClientOriginalName();
            $file_size = $request->file->getClientSize();
            $file_extension = $request->file->getClientOriginalExtension();
            $expensions = array('jpeg', 'jpg', 'png', 'gif');

            if(in_array($file_extension, $expensions) === false) {

                return redirect()->route('biografia')->with('invalido', 'Extensão não permitida, por favor escolha um arquivo JPEG ou PNG.');
            } else if($file_size > 2097152) {

                return redirect()->route('biografia')->with('invalido', 'O tamanho do arquivo não pode exceder 2 MB');
            } else {

                unlink('uploads/biografia/' .  $biografia->img);
                $request->file->move('uploads/biografia', $file_name);

                $biografia->titulo = $request->input('titulo');
                $biografia->texto = $request->input('descricao');
                $biografia->img = $file_name;
                $biografia->save();

                return redirect()->route('biografia')->with('mensagem', 'O slide foi atualizado com sucesso!');
            }
        }
    }
}
