<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use App\User;

use Closure;

class RegistrarSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!isset($_SESSION['usuario'])) {
            
            $this->registarUsuario();
        }

        return $next($request);
    }

    private function registarUsuario() {
        
        $user = Auth::user();
        $nome = $user->name;
        $iniciais = explode(' ', $nome);
            
        if(count($iniciais) > 1) {
            $iniciais = substr($iniciais[0], 0, 1) . substr($iniciais[1], 0, 1);
        } else {
            $iniciais = substr($iniciais[0], 0, 1);
        }

        $user = User::find($user->id);
        $user->visita = date("Y-m-d H:i:s");
        $user->save();

        $_SESSION['usuario'] = array('id' => $user->id, 'nome' => $nome, 'iniciais' => $iniciais);
    }
}
