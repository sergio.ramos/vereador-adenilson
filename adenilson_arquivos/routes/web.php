<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

// Rotas do topsapp
Route::group(['prefix' => '/', 'middleware' => 'dados.site'], function () {
    // home
    Route::get('/', 'Site\SiteController@index')->name('site.home');

    // biografia
    Route::get('/biografia', 'Site\SiteController@biografia')->name('site.biografia');

    // noticias
    Route::get('/noticias', 'Site\SiteController@noticias')->name('site.noticias');

    Route::post('/maisNoticias', 'Site\SiteController@maisNoticias')->name('site.maisNoticias');

    Route::get('/noticias/{link?}/{id?}', 'Site\SiteController@verNoticia')->name('site.abrir.noticia');

    // cafe com politica
    Route::get('/cafepolitica', 'Site\SiteController@cafepolitica')->name('site.cafepolitica');

    Route::get('/cafepolitica/{link?}/{id?}', 'Site\SiteController@verVideo')->name('site.abrir.video');

    Route::post('/maisVideos', 'Site\SiteController@maisVideos')->name('site.maisVideos');

    // edição
    Route::get('/edicao', 'Site\SiteController@edicao')->name('site.edicao');

    Route::get('/edicao/{id?}', 'Site\SiteController@verEdicao')->name('site.abrir.edicao');

    // contato
    Route::get('/contato', 'Site\SiteController@contato')->name('site.contato');

    Route::post('/contato/enviar-email', 'Site\SiteController@sendContatoEmail')->name('site.contato.email');
});

// Rotas do tip
Auth::routes(['register' => false]);

Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'registrar.session'], 'namespace' => 'Admin'], function(){
    // inicio rotas página principal
    Route::get('', 'PrincipalController@index')->name('admin.home');

    Route::post('atualizar-titulo', 'PrincipalController@atualizarTitulo')->name('atualizar.titulo');

    Route::get('alterar-senha', 'PrincipalController@viewAlterarSenha')->name('alterar.senha');
    
    Route::post('alterar-senha', 'PrincipalController@alterarSenha')->name('alterar.senha');
    // fim rotas página pricipal

    // inicio rota slides
    Route::get('slides','SlideController@slide')->name('slide');

    Route::get('slides/cadastrar-slide','SlideController@viewCadastrarSlide')->name('cadastrar.slide');

    Route::post('slides/cadastrar-slide','SlideController@cadastrarSlide')->name('cadastrar.slide');

    Route::get('slides/editar-slide/{id?}','SlideController@viewEditarSlide')->name('editar.slide');

    Route::post('slides/editar-slide/{id?}','SlideController@editarSlide')->name('editar.slide');

    Route::delete('slides/deletar-slide/{id?}','SlideController@deletarSlide')->name('deletar.slide');
    // fim rota slides

    // inicio rota biografia
    Route::get('biografia','BiografiaController@index')->name('biografia');

    Route::post('biografia/atualizar-dados', 'BiografiaController@atualizarDados')->name('atualizar.dados.biografia');
    // fim rota biografia

    // inicio rota pagina noticias
    Route::get('noticias','NoticiaController@noticias')->name('noticias');

    Route::get('noticias/cadastrar-noticia','NoticiaController@viewCadastrarNoticia')->name('cadastrar.noticia');

    Route::post('noticias/cadastrar-noticia/upload-imagens', 'NoticiaController@uploadImagens');

    Route::post('noticias/cadastrar-noticia/delete-imagens', 'NoticiaController@deleteImagens');

    Route::post('noticias/cadastrar-noticia','NoticiaController@cadastrarNoticia')->name('cadastrar.noticia');

    Route::get('noticias/editar-noticia/{id?}','NoticiaController@viewEditarNoticia')->name('editar.noticia');

    Route::get('noticias/editar-noticia/view-imagens/{id?}', 'NoticiaController@viewFilesNoticia');

    Route::post('noticias/editar-noticia/upload-imagens/{id?}', 'NoticiaController@editarUploadImagens');

    Route::post('noticias/editar-noticia/delete-imagens/{id?}', 'NoticiaController@editarDeleteImagens');

    Route::post('noticias/editar-noticia/{id?}','NoticiaController@editarNoticia')->name('editar.noticia');

    Route::delete('noticias/deletar-noticia/{id?}','NoticiaController@deletarNoticia')->name('deletar.noticia');
    // fim rota pagina noticias

    // inicio rota pagina café com política
    Route::get('videos','VideosController@videos')->name('videos');

    Route::get('videos/cadastrar-video','VideosController@viewCadastrarVideo')->name('cadastrar.video');

    Route::post('videos/cadastrar-video/upload-videos', 'VideosController@uploadVideos');

    Route::post('videos/cadastrar-video/delete-videos', 'VideosController@deleteVideos');

    Route::post('videos/cadastrar-video','VideosController@cadastrarVideo')->name('cadastrar.video');

    Route::get('videos/editar-video/{id?}', 'VideosController@viewEditarVideo')->name('editar.video');

    Route::get('videos/editar-video/view-videos/{id?}', 'VideosController@viewFilesVideo');

    Route::post('videos/editar-video/upload-videos/{id?}', 'VideosController@editarUploadVideos');

    Route::post('videos/editar-video/delete-videos/{id?}', 'VideosController@editarDeleteVideos');

    Route::post('videos/editar-video/{id?}','VideosController@editarVideo')->name('editar.video');

    Route::delete('videos/deletar-video/{id?}','VideosController@deletarVideo')->name('deletar.video');
    // fim rota pagina café com política

    // inicio rota contato
    Route::get('contato','ContatoController@contato')->name('contato');

    Route::get('contato/visualizar-mensagem/{id?}','ContatoController@visualizarMensagem')->name('visualizar.mensagem');
    // fim rota contato

    // inicio rota usuarios
    Route::get('usuarios','UsuarioController@usuarios')->name('usuarios');

    Route::get('usuarios/visualizar-usuario/{id?}', 'UsuarioController@visualizarUsuario')->name('visualizar.usuario');

    Route::get('usuario/cadastrar-usuario', 'UsuarioController@viewCadastrarUsuario')->name('cadatrar.usuario');

    Route::post('usuario/cadastrar-usuario', 'UsuarioController@cadastrarUsuario')->name('cadastrar.usuario');

    Route::get('usuario/editar-usuario/{id?}', 'UsuarioController@viewEditarUsuario')->name('editar.usuario');

    Route::post('usuario/editar-usuario/{id?}', 'UsuarioController@editarUsuario')->name('editar.usuario');

    Route::delete('usuarios/deletar-usuario/{id?}', 'UsuarioController@deletarUsuario')->name('deletar.usuario');
    // fim rota usuarios

    // inicio rota dados
    Route::get('dados','DadosController@dados')->name('dados.site');

    Route::post('dados', 'DadosController@atualizarDados')->name('atualizar.dados.site');
    // fim rota dados
});
