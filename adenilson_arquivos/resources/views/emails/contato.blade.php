<!DOCTYPE html>
<html>
<head>
    <title>Contato: Vereador Adenilson</title>
    <meta charset="utf-8">
    <style>
        body{
            width:510px;
            margin:0;
            padding:0 20px;
            font-family:Verdana, sans-serif;
            font-size:.8rem;
        }
        h1{
            font-size:1.2rem;
        }
        h2{
            font-size:1rem;
        }
    </style>
</head>
<body>
    <h1>Olá Vereador Adenilson você recebeu uma mensagem do seu site</h1>
<hr>
    <h2>Dados do solicitante</h2>
    <p>
        <strong>Nome:</strong> {{$nome}}<br>
        <strong>E-mail:</strong> {{$email}}<br>
        <strong>Telefone para contato:</strong> {{$telefone}}<br>
    </p>
<hr>
    <h2>Dados da mensagem</h2>
    <p>
        <strong>Assunto:</strong> {{$assunto}}<br>
        <strong>Mensagem:</strong><br> <?php echo nl2br($mensagem); ?>
    </p>
<hr>
<div>
    <p>
        <a href="http://adenilson.com.br" title="ADENILSON">.: Vereador Adenilson Rocha - PSDB :.</a>
        <br>
        Av. das Figueiras - Nº 1835<br>
        St. Comercial – CEP 78550-148<br>
        Sinop – MT – Brasil<br>
        Telefone: <a href="tel:(66) 3517-2800">(66) 3517-2800</a> - <a href="tel:(66) 3517-2801">(66) 3517-2801</a>
        Email: <a href="mailto:adenilson@topsinop.com.br">adenilson@topsinop.com.br</a>
    </p>
</div>
</body>
</html> 