@extends(('template.admin'))
@section('title')
    Vídeos
    @parent
@stop

@section('css_pagina')

	{{-- <link rel="stylesheet" href="{{ asset('css/cadastrar_recurso.css') }}"> --}}

@endsection
@section('conteudo')
<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
	<!-- begin:: Subheader -->
	<div class="kt-subheader   kt-grid__item" id="kt_subheader">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Vídeos</h3>
			<span class="kt-subheader__separator kt-hidden"></span>
			<div class="kt-subheader__breadcrumbs">
				<a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="{{ route('admin.home') }}" class="kt-subheader__breadcrumbs-link">Inicio</a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="{{ route('videos') }}" class="kt-subheader__breadcrumbs-link">Vídeos</a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="{{ route('cadastrar.video') }}" class="kt-subheader__breadcrumbs-link">Cadastrar Vídeo</a>
			</div>
		</div>
		<div class="kt-subheader__toolbar">
			<a href="{{route('site.cafepolitica')}}" class="btn btn-brand btn-elevate btn-icon-sm" target="_blank">
				<i class="fa fa-search-plus"></i> Visualizar Página
			</a>
		</div>
	</div>
	<!-- end:: Subheader -->

	<!-- begin:: Content -->
	<div class="kt-content">
		<div class="kt-grid__item kt-grid__item--fluid" id="kt_content">
			<div class="kt-portlet kt-portlet--mobile">
				<div class="kt-portlet__head kt-portlet__head--lg">
					<div class="kt-portlet__head-label">
						<span class="kt-portlet__head-icon">
							<i class="kt-font-brand fa fa-plus-square"></i>
						</span>
						<h3 class="kt-portlet__head-title">
							Cadastrar Vídeo
						</h3>
					</div>
				</div>
				<div class="kt-portlet__body">
					<div class="kt-widget14__header kt-margin-b-10">
						<h4 class="kt-widget14__title">
							Vídeo
						</h3>
						<span class="kt-widget14__desc">
							Cadastre um novo vídeo.
						</span>
					</div>
					
					<form id="video" class="kt-form kt-margin-t-10" method="POST" action="{{ route('cadastrar.video') }}" enctype="multipart/form-data">
						@csrf
						<div class="form-group mb-4">
							<label for="titulo_video">TÍTULO</label>
							<input type="text" class="form-control" placeholder="Dígite o título do vídeo" name="titulo_video" id="titulo_video">
						</div>
						<div class="form-group mb-4">
							<label for="data_video">DATA</label>
							<input type="datetime-local" class="form-control" placeholder="Dígite a data do vídeo" name="data_video" id="data_video">
						</div>
						<div class="form-group mb-4">
							<label for="imagem">FOTO DE CAPA</label>
							<div style="display: flex;">
								<input type="file" id="foto_capa" name="foto_capa" capture style="display: none">
								<input id="file-foto-capa" class="form-control mr-4" value="Selecione um arquivo" readonly>
								<button type="button" class="btn btn-brand" id="btn-foto-capa">Selecionar</button>
							</div>
						</div>
						<div class="form-group mb-4">
							<label for="dropzone">VIDEOS</label>
							<div class="kt-dropzone dropzone m-dropzone--success" id="dropzone">
								<div class="kt-dropzone__msg dz-message needsclick">
									<h3 class="kt-dropzone__msg-title">Arraste ou clique para fazer upload</h3>
									<span class="kt-dropzone__msg-desc">Apenas arquivos de video é permitido.</span>
								</div>
							</div>
							<input type="text" class="form-control" name="videos" id="videos" style="display: none">
						</div>
						<div class="form-group form-group-last mb-4">
							<label for="descricao_video">TEXTO</label>
							<textarea id="descricao_video" name="descricao_video"></textarea>
						</div>
						<div class="kt-form__actions">
							<button id="cadastrar-video" type="submit" class="btn btn-brand">Cadastrar Vídeo</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- end:: Content -->
</div>
@stop

@section('scripts_pagina')

	<script>

		$('#descricao_video').summernote({
			placeholder: 'Digite aqui seu texto',
			tabsize: 2,
			minHeight: 200 
		})

		$('form#video').validate({
			rules: {
				titulo_video: {
					required: true
				},
				data_video: {
					required: true
				},
				redator_video: {
					required: true
				},
				descricao_video: {
					required: true
				}

			}
		})

		let selecionarArquivo = () => {

			// Selecionar Arquivo foto capa
			let btnFotoCapa = $("button#btn-foto-capa")
			let inputFotoCapa = document.getElementById("foto_capa")

			btnFotoCapa.on("click", () => {
				
				inputFotoCapa.click()
			})

			$("#file-foto-capa").click(function() {

				inputFotoCapa.click()
			})

			inputFotoCapa.addEventListener("change", () => {

				let nome = "Não há arquivo selecionado!"
				if(inputFotoCapa.files.length > 0) nome = inputFotoCapa.files[0].name
				$("input#file-foto-capa").val(nome)
			})
			// Selecionar Arquivo foto capa
		}

		Dropzone.options.dropzone = {
			url: "cadastrar-video/upload-videos",
			headers: {
				'X-CSRF-Token': $('input[name="_token"]').val()
			},
            maxFilesize: 10,
            renameFile: function (file) {
                let data = new Date().toISOString().slice(0, 10)
                return data + "-" + file.name;
            },
            acceptedFiles: ".avi,.wmv,.mpeg,.mp4,.mkv,.flv",
            addRemoveLinks: true,
            timeout: 50000,
            removedfile: function (file) {

                let name = file.upload.filename;
                $.ajax({
                    headers: {
                        'X-CSRF-Token': $('input[name="_token"]').val()
                    },
                    type: 'POST',
                    url: 'cadastrar-video/delete-videos',
                    data: {filename: name},
                    success: function (data) {

						let imagens =  $("input#videos").val()

						$("input#videos").val(imagens.replace(data + ";", ""))

						// console.log("O arquivo " + data + " foi deletado com sucesso")
                    },
                    error: function (e) {
                        console.log(e)
                    }
                });
                let fileRef;
                return (fileRef = file.previewElement) != null ?
                    fileRef.parentNode.removeChild(file.previewElement) : void 0
            },
            success: function (file, response) {
				// console.log(response.arquivo)

				let imagens = $("input#videos").val()

				$("input#videos").val(imagens + response.arquivo + ";")
            },
            error: function (file, response) {
				// console.log(response)
                return false
            }
        }

		$(document).ready(function() {

			selecionarArquivo()
		})

		

	</script>
@endsection