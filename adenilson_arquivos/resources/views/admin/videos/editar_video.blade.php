@extends(('template.admin'))
@section('title')
    Vídeos
    @parent
@stop

@section('css_pagina')

	<style>
		
		.dz-image img {
			width: 100%;
		}
	
	</style>

@endsection
@section('conteudo')
<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
	<!-- begin:: Subheader -->
	<div class="kt-subheader   kt-grid__item" id="kt_subheader">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Vídeos</h3>
			<span class="kt-subheader__separator kt-hidden"></span>
			<div class="kt-subheader__breadcrumbs">
				<a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="{{ route('admin.home') }}" class="kt-subheader__breadcrumbs-link">Inicio</a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="{{ route('videos') }}" class="kt-subheader__breadcrumbs-link">Vídeos</a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="{{ route('editar.video', $video->id) }}" class="kt-subheader__breadcrumbs-link">Editar Vídeo</a>
			</div>
		</div>
		<div class="kt-subheader__toolbar">
			<a href="{{route('site.abrir.video', $video->link, $video->id)}}" class="btn btn-brand btn-elevate btn-icon-sm" target="_blank">
				<i class="fa fa-search-plus"></i> Visualizar Página
			</a>
		</div>
	</div>
	<!-- end:: Subheader -->

	<!-- begin:: Content -->
	<div class="kt-content">
		<div class="kt-grid__item kt-grid__item--fluid" id="kt_content">
			<div class="kt-portlet kt-portlet--mobile">
				<div class="kt-portlet__head kt-portlet__head--lg">
					<div class="kt-portlet__head-label">
						<span class="kt-portlet__head-icon">
							<i class="kt-font-brand fa fa-edit"></i>
						</span>
						<h3 class="kt-portlet__head-title">
							Editar Vídeo
						</h3>
					</div>
				</div>
				<div class="kt-portlet__body">
					<div class="kt-widget14__header kt-margin-b-10">
						<h4 class="kt-widget14__title">
							Vídeo
						</h3>
						<span class="kt-widget14__desc">
							Edite o Vídeo.
						</span>
					</div>
					<form id="video" class="kt-form kt-margin-t-10" method="POST" action="{{ route('editar.video') }}" enctype="multipart/form-data">
						@csrf
						<input type="number" name="id" value="{{ $video->id }}" style="display: none;">
						<div class="form-group mb-4">
							<label for="titulo_video">TÍTULO</label>
							<input type="text" class="form-control" placeholder="Dígite o título do vídeo" name="titulo_video" id="titulo_video" value="{{ $video->titulo }}">
						</div>
						<div class="form-group mb-4">
							<label for="data_video">DATA</label>
							<input type="datetime-local" class="form-control" placeholder="Dígite a data do vídeo" name="data_video" id="data_video" value="{{ $video->data }}">
						</div>
						<div class="form-group mb-4">
							<label for="imagem">FOTO DE CAPA</label>
							<div style="display: flex;">
								<input type="file" id="foto_capa" name="foto_capa" capture style="display: none">
								<input id="file-foto-capa" name="foto_capa_name" class="form-control mr-4" @if($video->imagem_capa) value="{{$video->imagem_capa}}" @else value="Selecione um arquivo" @endif readonly>
								<button type="button" class="btn btn-brand" id="btn-foto-capa">Selecionar</button>
							</div>
							@if($video->imagem_capa)
								<img src="{{ asset('uploads/videos/' . $video->link . '/' . $video->imagem_capa) }}" alt="foto de capa" style="max-width: 15rem;">
							@endif
						</div>
						<div class="form-group mb-4">
							<label for="dropzone">FOTOS</label>
							<div class="kt-dropzone dropzone m-dropzone--success" id="dropzone">
								<div class="kt-dropzone__msg dz-message needsclick">
									<h3 class="kt-dropzone__msg-title">Arraste ou clique para fazer upload</h3>
									<span class="kt-dropzone__msg-desc">Apenas arquivos de imagem é permitido.</span>
								</div>
							</div>
							<input type="text" class="form-control" name="videos" id="videos" style="display: none" value="{{$video->videos}}">
						</div>
						<div class="form-group form-group-last mb-4">
							<label for="descricao_video">TEXTO</label>
							<textarea id="descricao_video" name="descricao_video">{{ $video->descricao }}</textarea>
						</div>
						<div class="kt-form__actions">
							<button type="submit" class="btn btn-brand">Editar Vídeo</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- end:: Content -->
</div>
@stop

@section('scripts_pagina')

	<script>

		$('#descricao_video').summernote({
			placeholder: 'Digite aqui seu texto',
			tabsize: 2,
			minHeight: 200 
		})

		$('form#video').validate({
			rules: {
				titulo_video: {
					required: true
				},
				descricao_video: {
					required: true
				}

			}
		})

		let selecionarArquivo = () => {

			// Selecionar Arquivo foto capa
			let btnFotoCapa = $("button#btn-foto-capa")
			let inputFotoCapa = document.getElementById("foto_capa")

			btnFotoCapa.on("click", () => {
				
				inputFotoCapa.click()
			})

			$("#file-foto-capa").click(function() {

				inputFotoCapa.click()
			})

			inputFotoCapa.addEventListener("change", () => {

				let nome = "Não há arquivo selecionado!"
				if(inputFotoCapa.files.length > 0) nome = inputFotoCapa.files[0].name
				$("input#file-foto-capa").val(nome)
			})
			// Selecionar Arquivo foto capa
		}

		let idVideo = '{{$video->id}}'
		let linkVideo = '{{$video->link}}'
		let path = "{{asset('uploads/videos/')}}"
	
		Dropzone.options.dropzone = {
			url: "upload-videos/" + idVideo,
			headers: {
				'X-CSRF-Token': $('input[name="_token"]').val()
			},
            // maxFilesize: 10,
            renameFile: function (file) {
                let data = new Date().toISOString().slice(0, 10)
                return data + "-" + file.name;
            },
            acceptedFiles: ".avi,.wmv,.mpeg,.mp4,.mkv,.flv",
            addRemoveLinks: true,
            timeout: 50000,
			init: function () {
				thisDropzone = this

				$.get('view-videos/' + idVideo, function(data) {

					$.each(data, function(key,value){
						
						let mockFile = { name: value.name, size: value.size }

						thisDropzone.emit("addedfile", mockFile)

						thisDropzone.emit("thumbnail", mockFile, path + "/" + linkVideo + "/" + value.img)

						thisDropzone.emit("complete", mockFile)
					})
				})

				
			},
            removedfile: function (file) {
				let name

				if(typeof file.upload === 'undefined') {
					name = file.name
				} else {
					name = file.upload.filename
				}

                $.ajax({
                    headers: {
                        'X-CSRF-Token': $('input[name="_token"]').val()
                    },
                    type: 'POST',
                    url: 'delete-videos/' + idVideo,
                    data: {filename: name},
                    success: function (data) {

						let videos =  $("input#videos").val()

						data = data + ";"
						data = data.replace(/ /g,'')

						$("input#videos").val(videos.replace(data, ""))
                    },
                    error: function (e) {
                    }
                });
                let fileRef;
                return (fileRef = file.previewElement) != null ?
                    fileRef.parentNode.removeChild(file.previewElement) : void 0
            },

            success: function (file, response) {

				let videos = $("input#videos").val()

				$("input#videos").val(videos + response.arquivo + ";")

				thisDropzone.emit("thumbnail", file, path + "/" + linkVideo + "/" + response.img)
            },
            error: function (file, response) {
                return false
            }
        }

		$(document).ready(function() {

			selecionarArquivo()

		})

	</script>
@endsection