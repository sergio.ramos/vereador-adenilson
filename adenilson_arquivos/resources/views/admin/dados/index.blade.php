@extends(('template.admin'))
@section('title')
    Dados do site
    @parent
@stop
@section('conteudo')
<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
	<!-- begin:: Subheader -->
	<div class="kt-subheader   kt-grid__item" id="kt_subheader">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Dados do site</h3>
			<span class="kt-subheader__separator kt-hidden"></span>
			<div class="kt-subheader__breadcrumbs">
				<a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="{{ route('admin.home') }}" class="kt-subheader__breadcrumbs-link">Inicio</a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="{{ route('dados.site') }}" class="kt-subheader__breadcrumbs-link">Dados do site</a>
			</div>
		</div>
		<div class="kt-subheader__toolbar">
		</div>
	</div>
	<!-- end:: Subheader -->

	<!-- begin:: Content -->
	<div class="kt-content">
		<div class="kt-grid__item kt-grid__item--fluid" id="kt_content">
			<div class="kt-portlet kt-portlet--mobile">
				<div class="kt-portlet__head kt-portlet__head--lg">
					<div class="kt-portlet__head-label">
						<span class="kt-portlet__head-icon">
							<i class="kt-font-brand fas fa-atlas"></i>
						</span>
						<h3 class="kt-portlet__head-title">
							Dados do site
						</h3>
					</div>
				</div>
				<div class="kt-portlet__body">
					<div class="kt-widget14__header kt-margin-b-10">
						<h4 class="kt-widget14__title">
							Dados do site
						</h3>
						<span class="kt-widget14__desc">
							Edite os dados do site.
						</span>
					</div>
					@if(session('mensagem'))
						<div class="alert alert-success fade show mb-0 pl-3 pr-3 pt-1 pb-1" role="alert">
							<div class="alert-icon"><i class="la la-check-circle"></i></div>
							<div class="alert-text">{{ session('mensagem') }}</div>
							<div class="alert-close">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
											<span aria-hidden="true"><i class="la la-close"></i></span>
									</button>
							</div>
						</div>
					@endif
					<form id="site" class="kt-form kt-margin-t-10" method="POST" action="{{ route('atualizar.dados.site') }}" enctype="multipart/form-data">
						@csrf
						<div class="form-group mb-4">
							<label for="telefones">TELEFONE:</label>
							<input type="text" class="form-control" placeholder="(00) 0000-0000" name="telefones" id="telefones" value="{{ $site->telefones }}">
						</div>
						<div class="form-group mb-4">
							<label for="email">E-MAIL:</label>
							<input type="email" class="form-control" placeholder="Email..." name="email" id="email" value="{{ $site->email }}">
						</div>
						<div class="form-group mb-4">
							<label for="endereco">ENDEREÇO:</label>
							<input type="text" class="form-control" placeholder="Endereço..." name="endereco" id="endereco" value="{{ $site->endereco }}">
						</div>
						<div class="form-group mb-4">
							<label for="facebook">FACEBOOK:</label>
							<input type="text" class="form-control" placeholder="Facebook..." name="facebook" id="facebook" value="{{ $site->facebook }}">
						</div>
						<div class="form-group mb-4">
							<label for="twitter">TWITTER:</label>
							<input type="text" class="form-control" placeholder="Twitter..." name="twitter" id="twitter" value="{{ $site->twitter }}">
						</div>
						<div class="form-group mb-4">
							<label for="youtube">YOUTUBE:</label>
							<input type="text" class="form-control" placeholder="Youtube..." name="youtube" id="youtube" value="{{ $site->youtube }}">
						</div>
						<div class="form-group form-group-last mb-4">
							<label for="receber_info" style="width: 100%; margin: 0;">E-MAILS PARA RECEBER AS INFORMAÇÕES DE CONTATO</label>
							<span class="kt-widget14__desc">OBS: Colocar os e-mails sem espaço separados por ponto e vírgula, ex: email1@topsapp.com.br;         email2@topsapp.com.br</span>
							<input type="text" class="form-control" placeholder="Emails..." name="receber_info" id="receber_info" value="{{ $site->receber_info }}">
						</div>
						<div class="kt-form__actions">
							<button type="submit" class="btn btn-brand">Atualizar Dados</button>
						</div>
					</form>
				</div>
				<div class="kt-portlet__body kt-portlet__body--fit">
					<!--begin: Datatable -->
					<div class="kt-datatable" id="json_data"></div>
					<!--end: Datatable -->
				</div>
			</div>
		</div>
	</div>
	<!-- end:: Content -->
</div>
@stop

@section('scripts_pagina')

	<script>

		$(document).ready(function() {

			$('form#site').validate({
				rules: {
					telefones: {
						required: true
					},
					email: {
						required: true
					},
					endereco: {
						required: true
					},
					facebook: {
						required: true
					},
					twitter: {
						required: true
					},
					youtube: {
						required: true
					},
					receber_info: {
						required: true
					}

				}
			})
		})

		

	</script>
@endsection