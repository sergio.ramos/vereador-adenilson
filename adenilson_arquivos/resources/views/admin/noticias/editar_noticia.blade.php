@extends(('template.admin'))
@section('title')
    Notícias
    @parent
@stop

@section('css_pagina')

	<style>
		
		.dz-image img {
			width: 100%;
		}
	
	</style>

@endsection
@section('conteudo')
<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
	<!-- begin:: Subheader -->
	<div class="kt-subheader   kt-grid__item" id="kt_subheader">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Notícias</h3>
			<span class="kt-subheader__separator kt-hidden"></span>
			<div class="kt-subheader__breadcrumbs">
				<a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="{{ route('admin.home') }}" class="kt-subheader__breadcrumbs-link">Inicio</a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="{{ route('noticias') }}" class="kt-subheader__breadcrumbs-link">Notícias</a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="{{ route('editar.noticia', $noticia->id) }}" class="kt-subheader__breadcrumbs-link">Editar Notícia</a>
			</div>
		</div>
		<div class="kt-subheader__toolbar">
			<a href="{{route('site.abrir.noticia', $noticia->link, $noticia->id)}}" class="btn btn-brand btn-elevate btn-icon-sm" target="_blank">
				<i class="fa fa-search-plus"></i> Visualizar Página
			</a>
		</div>
	</div>
	<!-- end:: Subheader -->

	<!-- begin:: Content -->
	<div class="kt-content">
		<div class="kt-grid__item kt-grid__item--fluid" id="kt_content">
			<div class="kt-portlet kt-portlet--mobile">
				<div class="kt-portlet__head kt-portlet__head--lg">
					<div class="kt-portlet__head-label">
						<span class="kt-portlet__head-icon">
							<i class="kt-font-brand fa fa-edit"></i>
						</span>
						<h3 class="kt-portlet__head-title">
							Editar Notícia
						</h3>
					</div>
				</div>
				<div class="kt-portlet__body">
					<div class="kt-widget14__header kt-margin-b-10">
						<h4 class="kt-widget14__title">
							Notícia
						</h3>
						<span class="kt-widget14__desc">
							Edite a notícia.
						</span>
					</div>
					<form id="noticia" class="kt-form kt-margin-t-10" method="POST" action="{{ route('editar.noticia') }}" enctype="multipart/form-data">
						@csrf
						<input type="number" name="id" value="{{ $noticia->id }}" style="display: none;">
						<div class="form-group mb-4">
							<label for="titulo_noticia">TÍTULO</label>
							<input type="text" class="form-control" placeholder="Dígite o título da notícia" name="titulo_noticia" id="titulo_noticia" value="{{ $noticia->titulo }}">
						</div>
						<div class="form-group mb-4">
							<label for="data_noticia">DATA</label>
							<input type="datetime-local" class="form-control" placeholder="Dígite a data da notícia" name="data_noticia" id="data_noticia" value="{{ $noticia->data }}">
						</div>
						<div class="form-group mb-4">
							<label for="redator_noticia">REDATOR</label>
							<input type="text" class="form-control" placeholder="Dígite o redator da notícia" name="redator_noticia" id="redator_noticia" value="{{ $noticia->redator }}">
						</div>
						<div class="form-group mb-4">
							<label for="imagem">FOTO DE CAPA</label>
							<div style="display: flex;">
								<input type="file" id="foto_capa" name="foto_capa" capture style="display: none">
								<input id="file-foto-capa" name="foto_capa_name" class="form-control mr-4" @if($noticia->imagem_capa) value="{{$noticia->imagem_capa}}" @else value="Selecione um arquivo" @endif readonly>
								<button type="button" class="btn btn-brand" id="btn-foto-capa">Selecionar</button>
							</div>
							@if($noticia->imagem_capa)
								<img src="{{ asset('uploads/noticias/' . $noticia->link . '/' . $noticia->imagem_capa) }}" alt="foto de capa" style="max-width: 15rem;">
							@endif
						</div>
						<div class="form-group mb-4">
							<label for="dropzone">FOTOS</label>
							<div class="kt-dropzone dropzone m-dropzone--success" id="dropzone">
								<div class="kt-dropzone__msg dz-message needsclick">
									<h3 class="kt-dropzone__msg-title">Arraste ou clique para fazer upload</h3>
									<span class="kt-dropzone__msg-desc">Apenas arquivos de imagem é permitido.</span>
								</div>
							</div>
							<input type="text" class="form-control" name="imagens" id="imagens" style="display: none" value="{{$noticia->fotos}}">
						</div>
						<div class="form-group form-group-last mb-4">
							<label for="descricao_noticia">TEXTO</label>
							<textarea id="descricao_noticia" name="descricao_noticia">{{ $noticia->descricao }}</textarea>
						</div>
						<div class="kt-form__actions">
							<button type="submit" class="btn btn-brand">Editar Notícia</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- end:: Content -->
</div>
@stop

@section('scripts_pagina')

	<script>

		$('#descricao_noticia').summernote({
			placeholder: 'Digite aqui seu texto',
			tabsize: 2,
			minHeight: 200 
		})

		$('form#noticia').validate({
			rules: {
				titulo_noticia: {
					required: true
				},
				descricao_noticia: {
					required: true
				}

			}
		})

		let selecionarArquivo = () => {

			// Selecionar Arquivo foto capa
			let btnFotoCapa = $("button#btn-foto-capa")
			let inputFotoCapa = document.getElementById("foto_capa")

			btnFotoCapa.on("click", () => {
				
				inputFotoCapa.click()
			})

			$("#file-foto-capa").click(function() {

				inputFotoCapa.click()
			})

			inputFotoCapa.addEventListener("change", () => {

				let nome = "Não há arquivo selecionado!"
				if(inputFotoCapa.files.length > 0) nome = inputFotoCapa.files[0].name
				$("input#file-foto-capa").val(nome)
			})
			// Selecionar Arquivo foto capa
		}

		let idNoticia = '{{$noticia->id}}'
		let linkNoticia = '{{$noticia->link}}'
	
		Dropzone.options.dropzone = {
			url: "upload-imagens/" + idNoticia,
			headers: {
				'X-CSRF-Token': $('input[name="_token"]').val()
			},
            maxFilesize: 10,
            renameFile: function (file) {
                let data = new Date().toISOString().slice(0, 10)
                return data + "-" + file.name;
            },
            acceptedFiles: ".jpeg,.jpg,.png,.gif",
            addRemoveLinks: true,
            timeout: 50000,
			init: function () {
				thisDropzone = this

				$.get('view-imagens/' + idNoticia, function(data) {
					let path = "{{asset('uploads/noticias/')}}"
		
					$.each(data, function(key,value){

						let mockFile = { name: value.name, size: value.size }

						thisDropzone.emit("addedfile", mockFile)

						thisDropzone.emit("thumbnail", mockFile, path + "/" + linkNoticia + "/fotos/" + value.name)

						// thisDropzone.createThumbnailFromUrl(file, imageUrl, callback, crossOrigin)

						thisDropzone.emit("complete", mockFile)
					})
				})

				
			},
            removedfile: function (file) {
				let name

				if(typeof file.upload === 'undefined') {
					name = file.name
				} else {
					name = file.upload.filename
				}

                $.ajax({
                    headers: {
                        'X-CSRF-Token': $('input[name="_token"]').val()
                    },
                    type: 'POST',
                    url: 'delete-imagens/' + idNoticia,
                    data: {filename: name},
                    success: function (data) {

						let imagens =  $("input#imagens").val()

						data = data + ";"
						data = data.replace(/ /g,'')

						$("input#imagens").val(imagens.replace(data, ""))

						console.log($("input#imagens").val())
                    },
                    error: function (e) {
						alert('erro removed')
                    }
                });
                let fileRef;
                return (fileRef = file.previewElement) != null ?
                    fileRef.parentNode.removeChild(file.previewElement) : void 0
            },

            success: function (file, response) {

				let imagens = $("input#imagens").val()

				$("input#imagens").val(imagens + response.arquivo + ";")
            },
            error: function (file, response) {
				alert('erro')
                return false
            }
        }

		$(document).ready(function() {

			selecionarArquivo()

		})

	</script>
@endsection