<div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed ">
	<!-- begin:: Header Menu -->
	<button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
	<div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper">
	</div>
	<!-- end:: Header Menu -->

	<!-- begin:: Header Topbar -->
	<div class="kt-header__topbar">
		<!--begin: User Bar -->
		<div class="kt-header__topbar-item kt-header__topbar-item--user">
			<div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="0px,0px">
				<div class="kt-header__topbar-user">
					<span class="kt-header__topbar-welcome kt-hidden-mobile">Olá,</span>
					<span class="kt-header__topbar-username kt-hidden-mobile">
						{{ $usuario['nome'] }}
					</span>
					<!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
					<span class="kt-badge kt-badge--username kt-badge--unified-brand kt-badge--lg kt-badge--rounded kt-badge--bold">
						{{ $usuario['iniciais'] }}
					</span>
				</div>
			</div>
			<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-xl">
				<!--begin: Head -->
				<div class="kt-user-card kt-user-card--skin-dark kt-notification-item-padding-x" style="background-image: url({{asset('assets/media/misc/bg-1.jpg')}})">
					<div class="kt-user-card__avatar">
						<!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
						<span class="kt-badge kt-badge--lg kt-badge--rounded kt-badge--bold kt-font-light">{{ $usuario['iniciais'] }}</span>
					</div>
					<div class="kt-user-card__name">
						{{ $usuario['nome'] }}
					</div>
				</div>
				<!--end: Head -->

				<!--begin: Navigation -->
				<div class="kt-notification">
				<a href="{{ route('alterar.senha') }}" class="kt-notification__item">
						<div class="kt-notification__item-icon">
							<i class="flaticon2-lock kt-font-brand"></i>
						</div>
						<div class="kt-notification__item-details">
							<div class="kt-notification__item-title kt-font-bold">
								Alterar Senha
							</div>
							<div class="kt-notification__item-time">
								Aletar senha de acesso ao sistema
							</div>
						</div>
					</a>
					<div class="kt-notification__custom">
						<a href="{{ route('logout') }}" class="btn btn-label-brand btn-sm btn-bold btn-block" 
						onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{ __('Sair') }}</a>
					</div>

					<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
						@csrf
					</form>
				</div>
				<!--end: Navigation -->
			</div>
		</div>
		<!--end: User Bar -->
	</div>
	<!-- end:: Header Topbar -->
</div>