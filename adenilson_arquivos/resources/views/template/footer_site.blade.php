 <div class="kt-footer  kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop" id="kt_footer">
	{{-- <div class="kt-container  kt-container--fluid"> --}}
    <div class="container">
        <div class="kt-footer__menu row pb-2">
                <div class="rodape">
            
                        <div class="rodape-aln">
                            
                            <div class="atul">
        
                                <div class="atul-tt">Receba nossas atualizações</div>
                                
                                <div class="atul-icons">
                                
                                    <a href="contato/" class="atul-env">
                                    
                                        <img src="{{asset('img/home_61.png')}}">
                                        <span>Cadastre seu<br /> e-mail</span>
                                    
                                    </a>
                                    
                                    <a href="https://api.whatsapp.com/send?phone=556696160098&text=Desejo%20receber%20as%20atualizações%20relacionadas%20ao%20vereador%20Adenilson%20Rocha!" class="atul-wh" target="_blank">
                                    
                                        <img src="{{asset('img/home_57.png')}}">
                                        <span>Cadastre-se<br /> no Whatsapp</span>
                                    
                                    </a>
                                
                                </div>
        
                            </div>
                            
                            <div class="mapa-div">
                            
                                <div class="mapa-tt">Encontre-nos</div>
                                <iframe id="iframe" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d580.4464240096426!2d-55.5108900883878!3d-11.851888525316783!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x845f5600e75de13b!2sC%C3%A2mara+de+Vereadores+de+Sinop!5e0!3m2!1spt-BR!2sbr!4v1483989287643" allowfullscreen></iframe>
                            
                            </div>
                            
                            <div class="info-div">
                            
                                <div class="info-tt">Info Contato</div>
                                <div class="infos">
                                    
                                  {{-- @foreach($dadoss as $dadoss) --}}
                                
                                    <div class="info">
                                        <img src="{{asset('img/home_54.png')}}"><span> {{ $dados_site->endereco }} </span>
                                    </div>
                                    
                                    <div class="info">
                                        <img src="{{asset('img/home_65.png')}}"><span> {{ $dados_site->telefones }} </span>
                                    </div>
                                    
                                    <div class="info">
                                        <img src="{{asset('img/home_70.png')}}"><span> {{ $dados_site->email }} </span>
                                    </div>
                                    
                                    {{-- @endforeach --}}
                                
                                </div>
                            
                            </div>
                            
                        </div>
                    
                    </div>
        </div>
        <div class="kt-footer__copyright row justify-content-center ">
            &copy;Copyright 2019 Adenilson Rocha - PSDB - Sinop/MT - Todos os direitos reservados.
            {{-- 2019&nbsp;©&nbsp;<a href="http://multiwaretecnologia.com.br/" target="_blank" class="kt-link">Multiware Tecnologia</a> --}}
        </div>
    </div>
</div>	