<div class="container">
    <div class="header">
				
        <a href="{{ route('site.home') }}" class="imglink"><img class="h-img" src="{{asset('img/home_03.png')}}"></a>
            
        <div class="h-linha"></div>

        <div class="separate">
            <div class="mb-aln">
            
                <i class="material-icons dropdown">menu</i>
                <span class="h-span">menu</span>
                    
                <div class="dropdown-content" id="myDropdown">
                    <a href="{{route('site.biografia')}}">Biografia</a>
                    <a href="{{route('site.noticias')}}">Na mídia</a>
                    <a href="{{route('site.cafepolitica')}}">Café com politíca</a>
                    {{-- <a href="{{route('site.edicao')}}">Edição digital</a> --}}
                    <a href="{{route('site.contato')}}">Contato</a>
                </div>
                    
            </div>
                
            <form action="/busca" class="search">
                    
                <input class="pesquisar" type="text" placeholder="Pesquisar..." autocomplete="off" name="palavra" />
                    
                <div class="pesquisa">
                    <img src="{{asset('img/home_06.png')}}">
                </div>
                    
            </form>
        </div>
            
    </div>

</div>