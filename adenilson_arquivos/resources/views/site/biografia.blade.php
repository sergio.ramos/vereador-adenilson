@extends(('template.site'))

@section('title')
    {{ $titulo_site }}
    @parent
@stop

@section('morehead')

    <style>
		#kt_footer > div > div.kt-footer__menu.row.pb-2 {
            display: none;
        }
            
        .biografia-texto
        {
            margin-bottom: 50px;
        }
        
    </style>

@stop

@section('css_pagina')
	<link rel="stylesheet" href="{{asset('css/site/quem_somos.css')}}">
	
	<style>
		.biografia-banner {
    		width: 100%;
    		height: 600px;
    		background: url(../uploads/biografia/{{ $biografia->img }}) center center;
    		background-size: cover;
		}
	</style>
@endsection

@section('conteudo')
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
        <!-- begin:: Content -->
	    <div class="kt-content p-0 m-0 ">
			
			<div class="biografia-banner"></div>
            
            <div class="biografia">
            
                <div class="biografia-aln">
                
                    <div class="biografia-linha">
                
                        <div class="linha"></div>
                    
                    </div>
                    
                    <div class="biografia-texto">
                        
                        {!! $biografia->texto !!}
                        
                    </div>
                    
                </div>
            
            </div>

        </div>
    </div>
@endsection

@section('scripts_pagina')
    <script src="{{asset('js/menu_site.js')}}"></script>

    <script>

		// inicio configurações do menu
		if(janelaWidth >= 1024) {
			width = "11.8rem"
			position = "0"
		} else {
			width = "10.8rem"
			position = "0"
		}

		// menu style
		$('div.menu-line').css("width", width)
		$('div.menu-line').css("left", position)

		$('section.cont_sobre div').removeAttr('style')

        // executar no inicio
		if(janelaWidth >= 576) {
			configMenuDesktop(width, position)
		}

		if(janelaWidth < 576) {
			configMenuMobile(width, position)
		}
        // executar no inicio
        
        // executar quando for redimensionado
		$(window).on('resize', function() {
			janelaWidth = $(this).width()

			if(janelaWidth >= 1024) {
				width = "11.8rem"
				position = "0"
			} else {
				width = "10.8rem"
				position = "0"
			}

			if(janelaWidth >= 576) {
				configMenuDesktop(width, position)
			}

			if(janelaWidth < 576) {
				configMenuMobile()
			}
		})
		// executar quando for redimensionado
		// fim configurações do menu

    </script>

@endsection