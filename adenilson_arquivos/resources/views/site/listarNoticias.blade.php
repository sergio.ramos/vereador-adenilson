@extends(('template.site'))

@section('title')
    {{ $titulo_site }}
    @parent
@stop

@section('morehead')

        
        <style>
            
            #kt_footer > div > div.kt-footer__menu.row.pb-2 {
                display: none;
            }
        
            .biografia-aln
            {
                width: auto;
            }
            
            .noticias-linha
            {
                height: auto;
            }
            
            .contato-title
            {
                align-items: flex-end!important;
            }
            
        </style>

@stop

@section('css_pagina')
    <link rel="stylesheet" href="{{asset('css/site/clientes.css')}}">
@endsection

@section('conteudo')
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
        <!-- begin:: Content -->
	    <div class="kt-content p-0 m-0 ">
			<div class="biografia">
            
                <div class="biografia-aln">
                    
                    <div class="contato-title">Na mídia</div>
                
                    <div class="biografia-linha">
                
                        <div class="linha"></div>
                    
                    </div>
                    
                    <div class="biografia-texto">
                    
                        <div class="noticias">

				<div class="noticias-lista">
                           
                    @foreach($noticias_slice as $noticia)
				
					    <div class="quadro post" id="post_{{ $noticia->id }}">
								
                            <a href="{{route('site.abrir.noticia', ['link' => $noticia->link, 'id' => $noticia->id])}}" class="not-link">

                            <div class="img-noticia" style="background:url('{{ asset('uploads/noticias/' . $noticia->link . '/' .  $noticia->imagem_capa )}}') center center; background-size:cover;">
                                        
                                    <div class="hover-img"><i class="fa fa-newspaper-o" style="font-size:60px;color:#fff"></i></div>

							    </div>

							    <div class="not-t">
								    <span>{{ $noticia->titulo }}</span>
							    </div>
								
						    </a>

					    </div>
                            
                    @endforeach
							
				</div>
                            
                <div class="botao"><h1 class="load-more" id="button" style="text-align:center;">MOSTRAR MAIS</h1></div>
                <input type="hidden" id="row" value="0">
                <input type="hidden" id="all" value="{{ count($noticias) }}">
                            

					</div>
                        
                    </div>
                    
                </div>
            
            </div>
        </div>
    </div>
@endsection

@section('scripts_pagina')
    {{-- <script src="{{asset('js/menu_site.js')}}"></script> --}}

    <script>

        $(document).ready(function(){

            // Load more data
            $('.load-more').click(function(){
                let row = Number($('#row').val())
                let allcount = Number($('#all').val())
                row = row + 9

                if(row <= allcount){
                    $("#row").val(row)

                    $.ajax({
                        url: '{{ route("site.maisNoticias") }}',
                        type: 'POST',
                        data: {row:row},
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        beforeSend:function(){
                            $(".load-more").text("CARREGANDO...")
                        },
                        success: function(response){

                            // Setting little delay while displaying new content
                            setTimeout(function() {
                                // appending posts after last post with class="post"
                                $(".post:last").after(response).show().fadeIn("slow")

                                let rowno = row + 9

                                // checking row value is greater than allcount or not
                                if(rowno > allcount){

                                    $('.load-more').text("MOSTRAR MAIS")
                                    Swal.fire('Não há mais notícias para mostrar!')
                                    return
                                }else{
                                    $(".load-more").text("MOSTRAR MAIS")
                                }
                            }, 2000)
                        }
                    })
                }else{
                    Swal.fire('Não há mais notícias para mostrar!')

                }
            })
        })

    </script>

@endsection