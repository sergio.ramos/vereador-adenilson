@extends(('template.site'))
@section('title')
    {{ $titulo_site }}
    @parent
@stop

@section('css_pagina')
    <link rel="stylesheet" href="{{asset('css/site/home.css')}}">
@endsection

@section('morehead')
    <link href="https://vjs.zencdn.net/7.4.1/video-js.css" rel="stylesheet">
    <style>
        
        .noticias-linha
        {
            justify-content: center;
        }
        
    </style>
@endsection

@section('conteudo')
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
        <!-- begin:: Content -->
	    <div class="kt-content">
            
            <div class="banner">
					
				<ul class="rslides">

                    @foreach($slides as $slide) 
					    <li style="background: url('{{asset('uploads/slide/' . $slide->imagem )}}') no-repeat center center;background-size: cover;height:450px;"></li>
                    @endforeach

			  	</ul>
					
            </div>
            
            <div class="midias">
				
                <div class="midia">

                    <span>Na Mídia</span>
                    <font>Leia na íntegra notícias em que somos destaque, acompanhe nossos artigos e<br class="brl">descubra meu posicionamento sobre os principais temas!</font>

                </div>

                <div class="noticias">

                    <div class="noticias-linha">
                        
                        @for($i = $noticias->count(), $e = $i-3; $i > $e; $i--)
            
                        <div class="quadro">
                            
                            <a href="{{ route('site.abrir.noticia', ['link' => $noticias[$i-1]->link, 'id' => $noticias[$i-1]->id]) }}" class="not-link">

                                <div class="img-noticia" style="background:url('{{ asset('uploads/noticias/' . $noticias[$i-1]->link . '/' .  $noticias[$i-1]->imagem_capa )}}') no-repeat center center;background-size:cover;">
                                    
                                    <div class="hover-img"><i class="fa fa-newspaper-o" style="font-size:60px;color:#fff"></i></div>

                                </div>

                                <div class="not-t">
                                    <span>{{ $noticias[$i-1]->titulo }}</span>
                                </div>

                                <div class="share-data">
                                    
                                    <div class="share-aln">

                                        <a rel="nofollow" rel="noreferrer"class="btn twitter" href="javascript: void(0);" onClick="window.open('https://twitter.com/intent/tweet?original_referer=/noticia/{{ $noticias[$i-1]->link }}&source=tweetbutton&text={{ $noticias[$i-1]->titulo }}&url=noticia/{{ $noticias[$i-1]->link }}','{{ $noticias[$i-1]->titulo }}', 'toolbar=0, status=0, width=650, height=450');"><img src="{{ asset('img/home_17.png') }}"></a>
                                        
                                        <a rel="nofollow" rel="noreferrer"class="btn facebook" href="javascript: void(0);" onClick="window.open('http://www.facebook.com/sharer.php?u=noticia/{{ $noticias[$i-1]->link }}','{{ $noticias[$i-1]->titulo }}', 'toolbar=0, status=0, width=650, height=450');"><img src="{{ ('img/home_19.png') }}"></a>
                                        
                                        <a href="https://plus.google.com/share?url=/noticia/{{ $noticias[$i-1]->link }}" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=650,width=450');return false;">
                                            <img src="{{ asset('img/home_21.png') }}"></a>
                                
                                        <span>{{ $noticias[$i-1]->fotografo }}</span>
                                    
                                    </div>
                                </div>
                            </a>
                        </div>
                        @endfor
                    </div>
                </div>
            </div>

            <div class="torta-div">
					
                <div class="torta"></div>
                
            </div>

            <div class="banner-meio" style="background:url('{{ asset('img/banner_02.png') }}') center center;background-size:cover;">
					
                <a href="{{ route('site.biografia') }}" class="saiba">Saiba Mais <i class="fa fa-angle-right" style="font-size:20px;color:#009d3d;margin-left:20px;"></i></a>
                
            </div>

            <div class="torta-baixo"><div class="a"></div><div class="b"></div></div>
				
				<div class="mk">
					
					<div class="instagram">
						
						<div class="insta-t">Adenilson no instagram</div>
						<div class="insta-f"></div>
						
						<div class="sociais-insta">
							
							<div class="acompanhe-t">
								
								<p>Acompanhe</p>
								<span>nossas redes sociais:</span>
								
							</div>
							
							<div class="acompanhe-s">
                                
								<a href="{{ $dados_site->twitter }}" target="_blank" class="redonda" style="background:url('{{ asset('img/home_34.png') }}') center center;background-size:cover;"></a>
								<a href="{{ $dados_site->facebook }}" target="_blank" class="redonda" style="background:url('{{ asset('img/home_36.png') }}') center center;background-size:cover;"></a>
								<a href="{{ $dados_site->instagram }}" target="_blank" class="redonda" style="background:url('{{ asset('img/home_38.png') }}') center center;background-size:cover;"></a>
								<a href="{{ $dados_site->youtube }}" target="_blank" class="redonda" style="background:url('{{ asset('img/home_40.png') }}') center center;background-size:cover;"></a>
								
							</div>
							
						</div>
						
					</div>
					
					<div class="cafe">
						
						<div class="cafe-t">Café com Politíca</div>
						<div class="cafe-v">
                            <video width="420" height="340" id='my-video' class='video-js' controls preload='auto' poster='/uploads/videos/{{ $video->link }}/{{ $video->imagem_capa }}' data-setup='{}'>
                            <source src='/uploads/videos/{{$video->link}}/videos/{{ $video->videos[0] }}' type='video/mp4'>
                            <p class='vjs-no-js'>
                              To view this video please enable JavaScript, and consider upgrading to a web browser that
                              <a href='https://videojs.com/html5-video-support/' target='_blank'>supports HTML5 video</a>
                            </p>
                            </video>
						</div>
						
						<div class="cafe-vt"><a href="{{ route('site.abrir.video', [$video->link, $video->id]) }}"><span class="limit">{{ $video->titulo }}</span><br class="brl"> <span class="tt-v">Veja como foi!</span></a></div>
						
                        <div class="cfp-btn-div">
                        
                            <div class="cfp-btn-aln">
                                
                                <a href="cafepolitica" class="cfp-btn"><span>Ver Todos os Vídeos</span> <i class="fa fa-angle-right" style="font-size:20px;color:#009d3d;"></i></a>
                            
                            </div>
                        
                        </div>
                      
					</div>
					
				</div>

        </div>
    </div>
@endsection

@section('scripts_pagina')
    {{-- <script src="{{asset('js/menu_site.js')}}"></script> --}}

    {{-- scripts  --}}

    <script type="text/javascript" src="{{asset('js/responsiveslides.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/jquery.instagramFeed.min.js')}}"></script>

    <script>
        $(function() {
            $(".rslides").responsiveSlides({
  
                speed: 2000,
                timeout: 9000
  
            })
        })
    </script>

    <script>
        (function($){
            $(window).on('load', function(){
                $.instagramFeed({
                'username': 'adenilsonrochaoficial',
                'container': ".insta-f",
                'items': 3,
                'display_profile': false,
                'display_biography': false,
                'styling': false 
                })
            })
        })(jQuery)
    </script>

    {{-- scripts --}}

    <script src='https://vjs.zencdn.net/7.4.1/video.js'></script>

    <script src="https://vjs.zencdn.net/7.4.1/lang/pt-BR.js"></script>

@endsection