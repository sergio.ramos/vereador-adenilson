@extends(('template.site'))

@section('title')
    {{ $titulo_site }}
    @parent
@stop

@section('morehead')

        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
        <script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>
        <style>
            
            #kt_footer > div > div.kt-footer__menu.row.pb-2 {
                display: none;
            }
        
            .biografia-aln
            {
                width: auto;
            }
            
            .noticias-linha
            {
                height: auto;
            }
            
            .contato-title
            {
                align-items: flex-end!important;
            }

            /* .quadro:hover .hover-img
            {
                background: rgb(0, 0, 0, 0)!important;
            }
                
            .quadro:hover .hover-img-video
            {
                display: none!important;
            } */

            .hover-img-video
            {
                background: #ccc;
                /* display: none!important; */
            }

        </style>

@stop

@section('css_pagina')
    <link rel="stylesheet" href="{{asset('css/site/clientes.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
@endsection

@section('conteudo')
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
        <!-- begin:: Content -->
	    <div class="kt-content p-0 m-0 ">
			<div class="biografia">
            
                <div class="biografia-aln">
                    
                    <div class="contato-title">Edição Digital</div>
                
                    <div class="biografia-linha">
                
                        <div class="linha"></div>
                    
                    </div>
                    
                    <div class="biografia-texto">
                    
                        <div class="noticias">

                            <div class="noticias-linha">
                                    
                                @foreach($edicoes as $edicao)
                            
                                    <div class="quadro post" id="post_{{ $edicao->id }}">
                                            
                                        <a href="{{route('site.abrir.edicao', ['id' => $edicao->id])}}" class="not-link">

                                            <div class="img-noticia" style="background:url('uploads/ResizeBasic.php?filename={{ $edicao->imagem_capa }}&width=500&height=500') center center;background-size:cover;">

                                                <div class="hover-img-video"><i class="fa fa-newspaper-o" style="font-size:60px;color:#fff"></i></div>
                                                    
                                                {{-- <div class="hover-img"><i class="fa fa-newspaper-o" style="font-size:60px;color:#fff"></i></div> --}}

                                            </div>

                                            <div class="not-t">
                                                <span>{{ $edicao->titulo }}</span>
                                            </div>
                                            
                                        </a>

                                    </div>
                                        
                                @endforeach
                                        
                            </div>
                            
                            <div class="botao"><h1 class="load-more" id="button" style="text-align:center;">MOSTRAR MAIS</h1></div>
                            <input type="hidden" id="row" value="0">
                            <input type="hidden" id="all" value="{{ count($edicoes) }}">
                            
					    </div>
                    </div>
                    
                </div>
            
            </div>
        </div>
    </div>
@endsection

@section('scripts_pagina')
    {{-- <script src="{{asset('js/menu_site.js')}}"></script> --}}

    <script>

        // $(document).ready(function(){

        //     // Load more data
        //     $('.load-more').click(function(){
        //         let row = Number($('#row').val())
        //         let allcount = Number($('#all').val())
        //         row = row + 9

        //         if(row <= allcount){
        //             $("#row").val(row)

        //             $.ajax({
        //                 url: '{{ route("site.maisNoticias") }}',
        //                 type: 'POST',
        //                 data: {row:row},
        //                 headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        //                 beforeSend:function(){
        //                     $(".load-more").text("CARREGANDO...")
        //                 },
        //                 success: function(response){

        //                     // Setting little delay while displaying new content
        //                     setTimeout(function() {
        //                         // appending posts after last post with class="post"
        //                         $(".post:last").after(response).show().fadeIn("slow")

        //                         let rowno = row + 9

        //                         // checking row value is greater than allcount or not
        //                         if(rowno > allcount){

        //                             $('.load-more').text("MOSTRAR MAIS")
        //                             Swal.fire('Não há mais notícias para mostrar!')
        //                             return
        //                         }else{
        //                             $(".load-more").text("MOSTRAR MAIS")
        //                         }
        //                     }, 2000)
        //                 }
        //             })
        //         }else{
        //             Swal.fire('Não há mais notícias para mostrar!')

        //         }
        //     })
        // })

    </script>

@endsection