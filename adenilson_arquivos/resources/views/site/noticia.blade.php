@extends(('template.site'))

@section('title')
    {{ $titulo_site }}
    @parent
@stop

@section('morehead')

    <style>
        #kt_footer > div > div.kt-footer__menu.row.pb-2 {
            display: none;
        }
        
        .contato-title
        {
            margin-top: 50px;
            line-height: 1.2em;
        }
            
        @media screen and (max-width: 800px){
                
            /* .contato-title
            {
                font-size: 25px!important;
                margin-top: 30px!important;
                margin-bottom: 20px!important;
            } */
        }
            
        @media screen and (max-width: 600px){
                
            .biografia-linha
            {
                /* margin-bottom: 30px!important; */
            }
        }
            
        @media screen and (max-width: 499.98px){
                
            body > div.kt-grid.kt-grid--hor.kt-grid--root > div.kt-grid__item.kt-grid__item--fluid.kt-grid.kt-grid--hor > div > div > div > div.contato-title
            {
                /* font-size: 20px!important; */
                font-size: 2rem;
                margin: 2rem 0 0 0;
                line-height: 2rem;
            }
        }
            
    </style>

@stop

@section('css_pagina')
    <link rel="stylesheet" href="{{asset('css/magnific_popup.css')}}">
    <link rel="stylesheet" href="{{asset('css/site/noticias.css')}}">
@endsection

@section('conteudo')
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
        <!-- begin:: Content -->
	    <div class="kt-content p-0 m-0 ">
            <div class="biografia">
            
                <div class="biografia-aln">
                    
                    <div class="contato-title">{{ $noticia->titulo }}</div>
                
                    <div class="biografia-linha">
                
                        <div class="linha"></div>
                        <div class="noticia-infos">
                        
                            <span>{{ $noticia->fotografo }}</span> <span>Redator: {{ $noticia->local }}</span>
                    
                        </div>
                    
                    </div>
                    
                    @isset($noticia->imagem_capa)
                        <div class="noticia-banner">
                        
                            <div class="banner-noticia">
                            
                                <div class="nt-share">
                                
                                    <a rel="nofollow" rel="noreferrer"class="btn twitter" href="javascript: void(0);" onClick="window.open('https://twitter.com/intent/tweet?original_referer=/noticia/{{ $noticia->link }}&source=tweetbutton&text={{ $noticia->titulo }}&url=noticia/{{ $noticia->link }}','{{ $noticia->titulo }}', 'toolbar=0, status=0, width=650, height=450');"><img src="{{asset('img/home_17.png')}}"></a>
                                
                                    <a rel="nofollow" rel="noreferrer"class="btn facebook" href="javascript: void(0);" onClick="window.open('http://www.facebook.com/sharer.php?u=noticia/{{ $noticia->link }}','{{ $noticia->titulo }}', 'toolbar=0, status=0, width=650, height=450');"><img src="{{asset('img/home_19.png')}}"></a>
                                    
                                    <a href="https://plus.google.com/share?url=/noticia/{{ $noticia->link }}" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=650,width=450');return false;">
                                    <img src="{{asset('img/home_21.png')}}"></a>
                                    
                                </div>

                                <a href="{{asset('uploads/noticias/' . $noticia->link . '/' .  $noticia->imagem_capa)}}" class="image-link">
                                    <div class="hoverzoom">
                                        <img class="nt-img img-fluid" src="{{asset('uploads/noticias/' . $noticia->link . '/' . $noticia->imagem_capa)}}" alt="">
                                    </div>
                                </a>
                                
                            </div>
                            
                        </div>
                    @endisset
                    
                    <div class="biografia-texto">
                    
                        {!! $noticia->descricao !!}
                        
                    </div>
                    
                    @isset($noticia->fotos) 
                        @empty(!$noticia->fotos)
                            <div class="galeria">
                                @foreach ($noticia->fotos as $item)
                                    <a class="" href="{{asset('uploads/noticias/' . $noticia->link . '/fotos' . '/' . $item)}}">
                                        <img src="{{asset('uploads/noticias/' . $noticia->link . '/fotos' . '/' . $item)}}" alt="">
                                    </a>
                                @endforeach
                            </div>
                        @endempty
                    @endisset
                    
                </div>
            
            </div>
        </div>
    </div>
@endsection

@section('scripts_pagina')
    {{-- <script src="{{asset('js/menu_site.js')}}"></script> --}}
    <script src="{{asset('js/magnific_popup.js')}}"></script>

    <script>

        // MAGINIFIC POPUP

        $(document).ready(function() {
            $('.image-link').magnificPopup({type:'image'})
            $('div.galeria').magnificPopup({
                delegate: 'a', // child items selector, by clicking on it popup will open
                type: 'image',
                gallery:{enabled:true}
                // other options
            })
        })
        
    </script>

@endsection