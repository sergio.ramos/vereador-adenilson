@extends(('template.site'))

@section('title')
    {{ $titulo_site }}
    @parent
@stop

@section('morehead')

    {{-- <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script> --}}
    <link href="https://vjs.zencdn.net/7.4.1/video-js.css" rel="stylesheet">

    <style>

        #kt_footer > div > div.kt-footer__menu.row.pb-2 {
            display: none;
        }
            
        .noticias-linha
        {
            height: auto;
        }
            
        .quadro:hover .hover-img
        {
            background: rgb(0, 0, 0, 0)!important;
        }
            
        .quadro:hover .hover-img-video
        {
            display: none!important;
        }
            
        .contato-title
        {
            align-items: flex-end!important;
        }

        @media screen and (max-width: 499.98px){
                
            body > div.kt-grid.kt-grid--hor.kt-grid--root > div.kt-grid__item.kt-grid__item--fluid.kt-grid.kt-grid--hor > div > div > div > div.contato-title
            {
                /* font-size: 20px!important; */
                font-size: 2rem;
                margin: 2rem 0 0 0;
                line-height: 2rem;
            }

            body > div.kt-grid.kt-grid--hor.kt-grid--root > div.kt-grid__item.kt-grid__item--fluid.kt-grid.kt-grid--hor > div > div > div {
                padding-bottom: 2rem;
            }

            div.biografia-texto {
                padding: 0;
            }
        }
            
    </style>

@stop

@section('css_pagina')
    {{-- <link rel="stylesheet" href="{{asset('css/site/ouvidoria.css')}}"> --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
@endsection

@section('conteudo')
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
        <!-- begin:: Content -->
	    <div class="kt-content p-0 m-0 ">
            <div class="biografia">
            
                <div class="biografia-aln">
                    
                    <div class="contato-title">{{ $video->titulo }}</div>
                
                    <div class="biografia-linha">
                
                        <div class="linha"></div>

                        <div class="noticia-infos">
                        
                            <span>{{ $video->data_video }}</span>
                    
                        </div>
                    
                    </div>
                    
                    <div class="biografia-texto">
                    
                        {!! $video->descricao !!}

                    </div>

                    @isset($video->videos) 
                        @empty(!$video->videos)
                            <div class="cafe-v">
                                @foreach ($video->videos as $item)
                                <video {{--width="420" height="340"--}} id='my-video' class='video-js' controls preload='auto' poster='/uploads/videos/{{$video->link}}/{{ $video->imagem_capa }}' data-setup='{}'>
                                    <source src='/uploads/videos/{{$video->link}}/videos/{{ $item }}' type='video/mp4'>
                                    <p class='vjs-no-js'>
                                        To view this video please enable JavaScript, and consider upgrading to a web browser that
                                        <a href='https://videojs.com/html5-video-support/' target='_blank'>supports HTML5 video</a>
                                    </p>
                                </video>
                                @endforeach
                            </div>
                        @endempty
                    @endisset
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts_pagina')

    <script>


		
    </script>

    <script src='https://vjs.zencdn.net/7.4.1/video.js'></script>

    <script src="https://vjs.zencdn.net/7.4.1/lang/pt-BR.js"></script>

@endsection