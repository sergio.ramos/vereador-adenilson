@extends(('template.site'))

@section('title')
    {{ $titulo_site }}
    @parent
@stop

@section('morehead')

        <style>
            #kt_footer > div > div.kt-footer__menu.row.pb-2 > div > div > div.atul {
                display: none;
            }
        
            .atul
            {
                display: none!important;
            }
            
            .rodape-aln
            {
                justify-content: center!important;
            }
            
        </style>

@stop

@section('css_pagina')
    <link rel="stylesheet" href="{{asset('css/site/contato.css')}}">
@endsection

@section('conteudo')
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
        <!-- begin:: Content -->
	    <div class="kt-content p-0 m-0 ">
            <div class="biografia">
            
                <div class="biografia-aln">
                    
                    <div class="contato-title">Contato</div>
                
                    <div class="biografia-linha">
                
                        <div class="linha"></div>
                    
                    </div>
                    
                    <div class="biografia-texto">
                    
                        <form id="contato" class="form contato_form kt-form kt-margin-t-10" action="{{route('site.contato.email')}}" method="POST" data-anijs="if: scroll, on: window, do: bounceInLeft animated, before: scrollReveal, after: holdAnimClass">
                            @csrf

                            <div class="linha-form">
                            
                                <div class="input-div">
                                    
                                    <span class="input-title">Nome Completo</span>
                                
                                    <div class="input-aln">
                                        <input class="input form-control contato_form_input" placeholder="Dígite seu nome..." type="text" name="nome" id="nome">
                                    </div>
                                    
                                </div>
                                
                                <div class="input-div">
                                    
                                    <span class="input-title">E-mail</span>
                                
                                    <div class="input-aln">
                                        <input class="input form-control contato_form_input" placeholder="Dígite seu email..." type="email" name="email" id="email">
                                    </div>
                                    
                                </div>
                            
                            </div>
                            
                            <div class="linha-form">
                            
                                <div class="input-div">
                                    
                                    <span class="input-title">Telefone</span>
                                
                                    <div class="input-aln">
                                        <input class="input form-control contato_form_input" placeholder="Dígite seu telefone..." type="text" name="telefone" id="telefone">
                                    </div>
                                    
                                </div>
                                
                                <div class="input-div">
                                    
                                    <span class="input-title">Assunto</span>
                                
                                    <div class="input-aln">
                                        <input class="input form-control contato_form_input" placeholder="Sobre o que desja conversar?" type="text" name="assunto" id="assunto">
                                    </div>
                                    
                                </div>
                            
                            </div>
                            
                            <div class="linha-form msg">
                            
                                <div class="input-div msg">
                                    
                                    <span class="input-title msg">Mensagem</span>
                                
                                    <div class="input-aln msg">
                                        <textarea class="input msg form-control contato_form_input" name="mensagem" id="mensagem"></textarea>
                                    </div>
                                   
                                </div>
                            
                            </div>
                            
                            <div class="form-btns">

                                <div class="validation col-12 col-lg-6 col-md-7 mt-4 d-flex justify-content-start">
                                    <div class="g-recaptcha" data-sitekey="6LeGR84UAAAAAM2TFLs4A63z3dneZ6yHOo2XsAQc"></div>
                                </div>

                                <input class="btns" type="submit" id="ENVIO" value="Enviar">
                                <input class="btns contato_submit" id="ENVIO" type="reset" value="Limpar">
                               
                            </div>
                            <div class="mensagem_form">
                                @if(session('invalido'))
                                    <div class="alert alert-danger fade show mb-0 pl-3 pr-3 pt-1 pb-1" role="alert">
                                        <div class="alert-icon"><i class="la la-times-circle"></i></div>
                                        <div class="alert-text">{{ session('invalido') }}</div>
                                        <div class="alert-close">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true"><i class="la la-close"></i></span>
                                            </button>
                                        </div>
                                    </div>
                                    @elseif(session('mensagem'))
                                    <div class="alert alert-success fade show mb-0 pl-3 pr-3 pt-1 pb-1 col-12" role="alert">
                                        <div class="alert-icon"><i class="la la-check-circle"></i></div>
                                        <div class="alert-text">{{ session('mensagem') }}</div>
                                        <div class="alert-close">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <span aria-hidden="true"><i class="la la-close"></i></span>
                                                </button>
                                        </div>
                                    </div>
                                @endif
                            </div>
                            
                        </form>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts_pagina')
    {{-- <script src="{{asset('js/menu_site.js')}}"></script> --}}

    <script>
        
        // configurações do formulario
        $('form#contato').validate({
			rules: {
				nome: {
					required: true
				},
				email: {
					required: true
				},
                telefone: {
                    required: true
                },
                assunto: {
                    required: true
                },
                mensagem: {
                    required: true
                }
			}
		})

        $('input#telefone').inputmask("(99) 9 9999-9999")
        $("input#email").inputmask({mask:"*{1,20}[.*{1,20}][.*{1,20}][.*{1,20}]@*{1,20}[.*{2,6}][.*{1,2}]",greedy:!1,onBeforePaste:function(t,a){return(t=t.toLowerCase()).replace("mailto:","")},definitions:{"*":{validator:"[0-9A-Za-z!#$%&'*+/=?^_`{|}~-]",cardinality:1,casing:"lower"}}})

    </script>

    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>

@endsection