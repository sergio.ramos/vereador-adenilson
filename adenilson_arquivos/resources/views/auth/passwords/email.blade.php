@extends('layouts.app')

@section('css_pagina')
    <link rel="stylesheet" href="{{asset('css/resetar_password.css')}}">
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
            <div class="card">
                <div class="card-header d-flex justify-content-center align-items-center">
                    <img src="{{asset('assets/img/logo_tip.svg')}}" alt="logo topsapp">
                </div>

                <div class="card-body d-flex justify-content-center align-items-center">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group input-group">
                            <label for="email" class="email">{{ __('Recuperar senha') }}</label>
                            <span class="input-group-addon">
                                <i class="fa fa-envelope"></i>
                            </span>

                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group mb-3">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Enviar') }}
                            </button>
                        </div>
                        <div class="form-group mb-0">
                            <button type="button" class="btn btn-primary" onclick="javascript:location.href='{{route('login')}}'">
                                {{ __('Voltar') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
    </div>
</div>
@endsection
